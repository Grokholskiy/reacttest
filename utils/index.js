import { applyMiddleware, createStore } from 'redux';
import rootReducer from '../src/App';
import { middlewares } from '../src/App';



export const testStore = (initialState) => {
    const createStoreWithMiddleware = applyMiddleware(...middlewares)(createStore);
    return createStoreWithMiddleware(rootReducer, initialState);
};