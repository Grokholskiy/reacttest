import  React from "react";
import { DaySchedule } from "./DaySchedule"
import { week, maxLesson} from "../common/constants"


class WeekSchedule extends React.Component{
    render(){
        return(
            <>
                {week.map(day =>{
                    return <DaySchedule subjects={this.props.subjects}
                                        subjectLink={this.props.subjectLink}
                                        day={day}
                                        editConfig={this.props.editConfig}
                                        schedule={this.props[day.toUpperCase()]}
                                        maxLesson={maxLesson} key={day}/>
                    })
                }
            </>
        )
    }

}

export  { WeekSchedule }