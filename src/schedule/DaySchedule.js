import React from 'react';
import {connect} from "react-redux";
import { scheduleService } from "./Schedule.service"
import { Link } from "react-router-dom";


class DaySchedule extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            edit:false,
            schedule: props.schedule
        }

        this.subjectsRefs = {};
        props.maxLesson.forEach( (e,i) =>{
            i++;
            this.subjectsRefs[`lesson_${i}`]= React.createRef();
        })

        this.saveDaySchedule = this.saveDaySchedule.bind(this);
        this.editDaySchedule = this.editDaySchedule.bind(this);
    }


    editDaySchedule(){
        this.setState({edit:true})
    }

    handleChange(e){
        e.target.classList.remove("error");
    }

    saveDaySchedule(){
        if(this.props.editConfig.editable){
            let scheduleId = null;

            if(this.props.schedule){
                scheduleId = this.props.schedule.id
            }
            const schedule = {};

            schedule.id = scheduleId;
            schedule.day = this.props.day.toUpperCase();
            schedule.group = { id: this.props.editConfig.groupId };

            const daySchedule = [];

            Object.keys(this.subjectsRefs).forEach((key) => {

                const subjectNumber = key.replace("lesson_","");
                if(subjectNumber){
                    let schedule = null;

                    if(this.props.schedule && this.props.schedule.daySchedule){
                        schedule = this.props.schedule.daySchedule.find( lesson => lesson.number === subjectNumber);
                    }

                    if(!schedule){
                        schedule = {};
                        schedule.id = null;
                        schedule.subject = {};
                        schedule.number = subjectNumber;
                    }

                    schedule.subject.id = this.subjectsRefs[key].current.value === 0 ? null : this.subjectsRefs[key].current.value;
                    daySchedule.push(schedule);
                }
            });

            schedule.daySchedule = daySchedule;

            scheduleService.saveDaySchedule(schedule)
                .then(schedule =>{
                    this.setState({schedule, edit:false});
                }).catch(error =>{
                console.log(error);
                    if (error.error === "Validation error"){
                        error.errors.forEach( e => {
                             this.subjectsRefs[e.error].current.classList.add("error");
                        })
                    }
            })
        }
    }

    displaySubjects(schedule, number) {
        if (schedule) {
            if (schedule.daySchedule) {
                let lesson = schedule.daySchedule.find(l => l.number === number);
                if (lesson) {
                    return <td>{!this.state.edit &&
                            <>
                                {this.props.subjectLink &&
                                    <Link to={`${this.props.subjectLink}${lesson.subject.id}/${schedule.group.id}/${schedule.day}/${number}`}>{lesson.subject.name}</Link>
                                }

                                {!this.props.subjectLink &&
                                    lesson.subject.name
                                }
                            </>
                        }
                        {this.props.editConfig.editable &&
                            <SujectsSelect onChange={this.handleChange}
                                           subjects={this.props.subjects}
                                           selectedSubject={lesson.subject}
                                           number={number}
                                           edit={this.state.edit}
                                           ref={this.subjectsRefs[`lesson_${number}`]}
                                           groupId={this.props.editConfig.groupId}
                            />
                        }
                    </td>
                }
            }
        }

        return <td>
                    {this.props.editConfig.editable &&
                        <SujectsSelect onChange={this.handleChange}
                                       subjects={this.props.subjects}
                                       selectedSubject={null}
                                       number={number}
                                       edit={this.state.edit}
                                       ref={this.subjectsRefs[`lesson_${number}`]}
                                       groupId={this.props.editConfig.groupId}
                        />
                    }
               </td>
    }

    render(){
        const {day, maxLesson ,  editConfig } = this.props;
        const { edit, schedule } = this.state;
        return(
            <>
                <table className="table-sm table-striped table-bordered table-hover">
                    <thead>
                    <tr>
                        <th colSpan={2}>
                            <div className="schedule-table-header">
                                <h4>{day}</h4>
                                {editConfig.editable &&
                                    <>
                                        {!edit &&
                                            <div className="editBtn" onClick={this.editDaySchedule} title="Edit"></div>
                                        }
                                        {edit &&
                                            <div className="saveBtn" onClick={this.saveDaySchedule} title="Save"></div>
                                        }
                                    </>
                                }
                            </div>
                        </th>
                    </tr>
                    </thead>
                    <tbody>
                        {maxLesson.map((e,i) =>{
                            i++;
                            return <tr key={i}>
                                        <td>{i}</td>
                                        {
                                            this.displaySubjects(schedule, i)
                                        }
                                    </tr>
                            })
                        }
                    </tbody>
                </table>
            </>
        )
    }
}

const SujectsSelect = React.forwardRef((props, ref) => {
    const {subjects, selectedSubject, number, edit, onChange,groupId } = props;
    if(!edit){
        return null;
    }
    return(
        <>
            <select ref={ref} name={`subject_${number}`} onChange={onChange} defaultValue={ selectedSubject ? selectedSubject.id : 0 }>
                <option value="0" key={0}>Select subject</option>
                {subjects &&
                    subjects.map((subject) => {
                        if(subject.group && subject.group.id == groupId){
                            if(subject.teacher)
                                return <option value={subject.id} key={subject.id} >{subject.name} </option>
                        }
                    })
                }
            </select>
        </>
    )
})


function mapStateToProps(state) {
    const { subjects } = state.subjects;
    return {
        subjects
    };
}

const connectedWeekSchedule = connect(mapStateToProps)( DaySchedule );
export  { connectedWeekSchedule as DaySchedule}