import {apiUrl} from "../common/constants";
import {handle} from "../common/service/responceHendler";
import RequestOpt from "../common/service/requestOptionBuilder";

export const scheduleService = {
    getGroupSchedule,
    saveDaySchedule,
    getTeacherSchedule
}

function getGroupSchedule(groupId){
    return handle(
        fetch(`${apiUrl}schedule/group/${groupId}`,new RequestOpt().GET().AUTH())
    );
}

function getTeacherSchedule(){
    return handle(
        fetch(`${apiUrl}teacher/schedule`,new RequestOpt().GET().AUTH())
    );
}

function saveDaySchedule(schedule){
    return handle(
        fetch(`${apiUrl}schedule`, new RequestOpt().PATCH().AUTH().JSON().BODY(schedule))
    );
}