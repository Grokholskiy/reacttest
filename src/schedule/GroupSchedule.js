import React from 'react';
import { scheduleService } from './Schedule.service';
import {WeekSchedule} from "./WeekSchedule";
import {Loading} from "../common/loading/Loading";
import {Error} from "../common/error/Error";

class GroupSchedule extends React.Component{
    constructor(props){
        super(props)
        this.state = {
            schedule:[],
            loading:false,
            error:''
        };

    }

    componentDidMount() {
        this.groupId = this.props.match.params.id;
        this.loadGroupSchedule(this.groupId);
    }

    loadGroupSchedule(groupId){
        this.setState({loading:true});
        scheduleService.getGroupSchedule(groupId )
            .then(schedule =>{
                    const scheduleMap = {};
                    schedule.map(day=> scheduleMap[day.day] = day);

                    this.setState({loading:false, schedule:scheduleMap});
                },
                error => {
                    console.log(error);
                    this.setState({loading:true, error:error});
                }
            )
    }


    componentWillReceiveProps(nextProps, nextContext) {
        if(this.groupId && (this.groupId !== nextProps.match.params.id )){
            this.groupId = nextProps.match.params.id;
            this.loadGroupSchedule( this.groupId);
        }
    }


    render(){
        const { loading,error, schedule } = this.state;

        if(loading){
            return  <Loading />
        }else if(error){
            return <Error error={error} />
        }

        return (
            <div className="d-flex flex-column align-items-center">
                <h1>Schedule</h1>
                <div className="schedule-table">
                    <WeekSchedule {...schedule} editConfig={{editable:true, groupId:this.groupId}}/>
                </div>
            </div>
        );
    }
}

export  { GroupSchedule }