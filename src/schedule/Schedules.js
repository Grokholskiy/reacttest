import React from 'react';
import './Schedules.css'
import {connect} from "react-redux";
import { NavLink, Route } from 'react-router-dom';
import { GroupSchedule } from './GroupSchedule';
import {LangRoute} from "../languages/LangRoute";
import {LangLink} from "../languages/LangLink";

class Schedules extends React.Component{
    constructor(props) {
        super(props);
        this.state = {};
    }



    render(){
        const { groups } = this.props;

        return(
            <div className="schedule-page d-flex flex-column align-items-center">
                <h1>Schedule page</h1>

                <div>
                    <h4>Select Group:</h4>

                    {groups &&
                        <ul className="groups">
                            {
                                groups.map( group =>{
                                    return <li key={group.id}>
                                        <LangLink to={`/director/schedule/group/${group.id}`} activeClassName='active'>{group.name}</LangLink>
                                    </li>
                                })
                            }
                        </ul>
                    }
                </div>
                <div>
                    <LangRoute path="/director/schedule/group/:id" component={ GroupSchedule } />
                </div>

            </div>
        )
    }
}

function mapStateToProps(state) {
    const { groups } = state.groups;
    return {
        groups
    };
}
const connectedSchedule = connect(mapStateToProps)(Schedules);
export  { connectedSchedule as Schedule}