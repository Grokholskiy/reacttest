import React from 'react';
import { Header } from "../header/Header";
import { TeacherHeader } from "./TeacherHeader";
import { User } from "../common/user/User";
import { TeacherSchedule } from "./TeacherSchedule";
import AccessControl from "../common/AccessControl";
import NoAccess from "../common/NoAccess";
import {Subject} from "./subject/Subject";
import { week, maxLesson } from "../common/constants";
import {Lesson} from "./lesson/Lesson";
import {LangRoute} from "../languages/LangRoute";


class Teacher extends React.Component {

    render(){
        return(
            <AccessControl
                allowedPermissions={["TEACHER"]}
                renderNoAccess={ () => <NoAccess /> }
            >
                <div>
                    <Header subHeader={ TeacherHeader } />

                    <LangRoute path="/teacher/schedule"  component={ TeacherSchedule } />
                    <LangRoute path={`/teacher/subject/:id/:group(\\d+)/:day(${week.join("|").toUpperCase()})/:number([1-${maxLesson.length}])`} component={ Subject } />
                    <LangRoute path="/teacher/subject/:subjectId/lesson/:lessonId"  component={ Lesson } />
                    <LangRoute path="/:user/mypage" render={(props) => {
                        return <User mypage={true} {...props}/>}
                    } />
                </div>
            </AccessControl>
        )
    }
}


export  { Teacher };