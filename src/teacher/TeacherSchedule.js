import React from 'react';
import { scheduleService } from '../schedule/Schedule.service';
import {WeekSchedule} from "../schedule/WeekSchedule";

class TeacherSchedule extends React.Component{
    constructor(props){
        super(props)
        this.state = {
            schedule:[],
            loading:false,
            error:''
        };

    }

    componentDidMount() {
        this.groupId = this.props.match.params.id;
        this.loadTeacherSchedule(this.groupId);
    }

    loadTeacherSchedule(groupId){
        this.setState({loading:true});
        scheduleService.getTeacherSchedule(groupId )
            .then(schedule =>{
                    const scheduleMap = {};
                    schedule.map(day=> scheduleMap[day.day] = day);

                    this.setState({loading:false, schedule:scheduleMap});
                },
                error => {
                    console.log(error);
                    this.setState({loading:true, error:error});
                }
            )
    }


    render(){
        const { loading, schedule } = this.state;

        if(loading){
            return <h3>Loading...</h3>
        }

        return (
            <div className="d-flex flex-column align-items-center">
                <h1>Schedule</h1>
                <div className="schedule-table">
                    <WeekSchedule {...schedule} editConfig={{editable:false}} subjectLink={"/teacher/subject/"}/>
                </div>
            </div>
        );
    }
}

export  { TeacherSchedule  }