import {apiUrl} from "../../common/constants";
import {handle} from "../../common/service/responceHendler";
import RequestOpt from "../../common/service/requestOptionBuilder";

export const subjectService = {
    getSubject
}

function getSubject(id){
    return handle(
        fetch(`${apiUrl}teacher/subject/${id}`,new RequestOpt().GET().AUTH().JSON())
    );
}