import React from 'react';
import { subjectService } from "./Subject.service";
import { Error } from "../../common/error/Error";
import { Loading } from "../../common/loading/Loading";
import { Lessons } from "../lesson/Lessons";
import { LessonAdd } from "../lesson/Lesson.add";
import {Button } from "react-bootstrap";



class Subject extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            loading:true,
            error:'',
            subject:'',
            createLesson:false

        }

        this.createLesson = this.createLesson.bind(this);
        this.cancelLesson = this.cancelLesson.bind(this);
        this.saveLesson = this.saveLesson.bind(this);
    }

    createLesson(){
        this.setState({createLesson:true});
    }

    cancelLesson(){
        this.setState({createLesson:false});
    }

    saveLesson(lesson){
        const subject = this.state.subject;
        subject.lessons.push(lesson);

        this.setState({subject, createLesson:false});
    }


    componentDidMount() {
        const { id, day, number, group } = this.props.match.params;

        this.setState({loading:true});

        subjectService.getSubject(id, day, number, group)
            .then(subject =>{
                console.log(subject);
                this.setState({loading:false, subject});
            })
            .catch(error =>{
                console.log(error);
                this.setState({error, loading:false});
            })
    }


    render(){
        const { day, number } = this.props.match.params;
        const { loading, error, subject, createLesson } = this.state;

        if(loading){
            return  <Loading />
        }else if(error){
            return <Error error={error} />
        }

        return (
            <div className="subject-page d-flex flex-column">
                <div className="text-center">
                    <h2>{subject.name}</h2>
                    <p>{subject.group &&
                        <>
                            Group - {subject.group.name}
                        </>
                    }</p>
                    <p>{day}, Lesson #{number}</p>
                </div>

                <div className="text-center">
                    {createLesson &&
                        <LessonAdd save={this.saveLesson} close={this.cancelLesson} data={this.props.match.params}/>
                    }

                    {!createLesson &&
                        <Button variant="primary" onClick={this.createLesson}>
                            Create Subject
                        </Button>
                    }
                    <Lessons subject={subject}/>
                </div>
            </div>
        )
    }
}

export { Subject }