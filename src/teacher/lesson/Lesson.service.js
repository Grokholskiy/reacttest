import {apiUrl} from "../../common/constants";
import {handle, ResponseType} from "../../common/service/responceHendler";
import RequestOpt from "../../common/service/requestOptionBuilder";

export const lessonService = {
    getAll:getAllLessons,
    getOne:getAllLessons,
    getOneWithMarks:getLessonWithMarks,
    save,
    edit,
    addMark
}

function getAllLessons(subjectId){
    return handle(
        fetch(`${apiUrl}teacher/subject/${subjectId}/lessons`,new RequestOpt().GET().AUTH())
    );
}

function getLessonWithMarks(lessonId, subjectId){
    return handle(
        fetch(`${apiUrl}teacher/subject/${subjectId}/lesson/${lessonId}`,new RequestOpt().GET().AUTH().JSON())
    );
}

function save(lesson){
    return handle(
        fetch(`${apiUrl}teacher/subject/lesson}`,new RequestOpt().PUT().AUTH().JSON().BODY(lesson))
    );
}

function edit(lesson){
    return handle(
        fetch(`${apiUrl}teacher/subject/lesson`,new RequestOpt().PATCH().AUTH().JSON().BODY(lesson))
    );
}

function addMark(mark){
    return handle(
        fetch(`${apiUrl}teacher/subject/lesson/mark`,new RequestOpt().PUT().AUTH().JSON().BODY(mark)),
        {responseType:ResponseType.VOID}
    );
}