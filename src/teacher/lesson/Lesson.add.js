import React from "react";
import Form from "react-bootstrap/Form";
import { Button, Col, Row } from "react-bootstrap";
import DatePicker from "react-datepicker";
import { lessonService } from "./Lesson.service";
import { FormValidation } from "../../common/formValidation/FormValidation"


import "react-datepicker/dist/react-datepicker.css";
import "./Lessons.css"

class LessonAdd extends React.Component{
    constructor(props){
        super(props);

        this.state = {
            submited:false,
            loading:false,
            saved:false,
            title:'',
            date:new Date()
        }

        this.titleRef = React.createRef();

        this.handleSelectDate = this.handleSelectDate.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.save = this.save.bind(this);
    }

    handleChange(e) {
        const { name, value } = e.target;
        this.setState({[name]: value})
    }
    handleSelectDate(date){
        this.setState({date});
    }


    save(event){
        event.preventDefault();
        this.setState({submited:true})

        if(event.target.checkValidity()){
            const { title, date } = this.state;
            const { data } = this.props;

            lessonService.save({
                title, date, number:data.number, subject:{id:data.id}
            }).then(lesson =>{
                console.log(lesson)
                this.props.save(lesson);
            }).catch(error =>{
                console.log(error);
            })
        }

    }

    componentDidMount() {
    }

    render(){
    const { submited, title } = this.state;

        return(
            <div className="container-fluid">
                    <Row className="justify-content-center"><h1>Create Lesson</h1></Row>
                    <Row className="justify-content-center">
                        <div className="col-12 col-sm-10 col-md-8 col-xl-6">
                            <FormValidation >
                                <Form noValidate onSubmit={this.save}  className={submited ? "was-validated":""} >
                                    <Form.Row>
                                        <Form.Group as={Col}>
                                            <Form.Label>Lesson Title</Form.Label>
                                            <Form.Control
                                                ref={this.titleRef}
                                                style={{textAlign:'center'}}
                                                type="text"
                                                name="title"
                                                value={title}
                                                onChange={this.handleChange}
                                                required
                                            />
                                            <div className="invalid-feedback">Please provide a valid name.</div>
                                        </Form.Group>
                                    </Form.Row>

                                    <Form.Row>
                                        <Form.Group as={Col}>
                                            <Form.Label>Lesson Date</Form.Label>
                                            <DatePicker
                                                dateFormat="yyyy-MM-dd"
                                                required={true}
                                                className="form-control"
                                                selected={this.state.date}
                                                onSelect={this.handleSelectDate}
                                                onChange={this.handleSelectDate}
                                            />
                                            <div className="invalid-feedback">Please provide a valid date.</div>
                                        </Form.Group>
                                    </Form.Row>

                                    <Row className="justify-content-center">
                                        <Button variant="secondary" onClick={this.props.close}>
                                            Cancel
                                        </Button>
                                        <Button variant="primary" type="submit">
                                            Create
                                        </Button>
                                    </Row>
                                </Form>
                            </FormValidation>
                        </div>
                    </Row>

            </div>
        )
    }
}

export { LessonAdd }