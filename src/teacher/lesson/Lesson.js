import React from "react";
import { lessonService } from "./Lesson.service";
import {Loading} from "../../common/loading/Loading";
import {Error} from "../../common/error/Error";
import {Table} from "react-bootstrap";
import { marks } from "../../common/constants"

class Lesson extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            loading:true,
            error:''
        }

    }


    componentDidMount() {
        const { lessonId, subjectId } = this.props.match.params;

        lessonService.getOneWithMarks(lessonId, subjectId)
            .then(lesson=>{
                this.setState({loading:false, lesson})
                console.log(lesson);
            }).catch(error=>{
                this.setState({loading:false, error})
                console.log(error)
            })
    }

    render(){
        const {loading, error, lesson} = this.state;

        if(loading){
            return  <Loading />
        }else if(error){
            return <Error error={error} />
        }

        return (
            <>
                <div className="lesson-page d-flex flex-column text-center">
                    <h1>Lesson #{lesson.id}</h1>

                    <div className="row m-0 justify-content-center">
                        <Table striped bordered hover size="sm" className="col-sm-11 col-md-10 col-xl-8">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Name</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                            {lesson.studenstMark &&
                                lesson.studenstMark.map((mark, i) => {
                                    return <tr key={lesson.id}>
                                        <td>{i+1}</td>
                                        <td>
                                           {mark.student.name} {mark.student.lastName}
                                        </td>
                                        <td>
                                            <MarkSelect lesson={lesson.id} student={mark.student.id} mark={mark.mark}/>
                                        </td>
                                    </tr>
                                })
                            }
                            </tbody>
                        </Table>
                    </div>
                </div>
            </>
        )
    }
}

const MarkSelect = ({lesson,student, mark})=>{
    function saveMark(mark, lessonId, studentId){
        console.log(arguments);
        lessonService.addMark({mark, lessonId, studentId})
            .then(() =>{

            })
            .catch(error =>{
                console.log(error);
            })
    }

    return (
        <select onChange={(e)=>{saveMark(e.target.value,lesson,student)}} value={mark ? mark : ""}>
            <option></option>
            {marks.map((mark)=> {
                return <option value={mark.key}>{mark.value}</option>
            })}

        </select>
    )
}

export { Lesson }