import React from "react";
import {Table} from "react-bootstrap";
import { Link } from "react-router-dom"

class Lessons extends React.Component{
    constructor(props){
        super(props);
        this.state = {}
    }


    componentDidMount() {
        // const { subjectId } = this.props;
        //
        // lessonService.getAll(subjectId)
        //     .then(lessons => {
        //         this.setState({loading:false, lessons})
        //         console.log(lessons);
        //     }).catch(error =>{
        //         this.setState({error});
        // })

    }

    render() {
        const { subject } = this.props;


        return (
            <>
                <h1>
                    Lessons
                </h1>

                <div className="row m-0 justify-content-center">
                    <Table striped bordered hover size="sm" className="col-sm-11 col-md-10 col-xl-8">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Title</th>
                            <th>Date</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        {subject.lessons &&
                            subject.lessons.map((lesson, i) => {
                                return <tr key={lesson.id}>
                                    <td>{i+1}</td>
                                    <td>
                                        <Link to={`/teacher/subject/${subject.id}/lesson/${lesson.id}`}>{lesson.title}</Link>
                                    </td>
                                    <td>{lesson.date.join("-")}</td>
                                    <td className="action">
                                        <div className="editBtn" title="Edit"></div>
                                        <div className="delBtn" title="Delete"></div>
                                    </td>
                                </tr>
                            })
                        }
                        </tbody>
                    </Table>
                </div>
            </>
        )
    }
}

export { Lessons }