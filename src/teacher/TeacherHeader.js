import React from 'react';
import { NavLink } from "react-router-dom";
import {LangLink} from "../languages/LangLink";

function TeacherHeader() {

    return (
            <div>
                <li>
                    <LangLink to="/teacher/mypage" activeClassName="active">Teacher</LangLink>

                </li>
                <li>
                    <LangLink to="/teacher/subjects" activeClassName="active">My subjects</LangLink>
                </li>
                <li>
                    <LangLink to="/teacher/schedule" activeClassName="active">My Schedule</LangLink>
                </li>
            </div>
    )
}

export { TeacherHeader };
