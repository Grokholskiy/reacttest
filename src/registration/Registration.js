import React from 'react';

import "../login/Login.css"
import './Registration.css';
import { registrationService } from "./registration.service";
import {Link} from "react-router-dom";
import {FormValidation} from "../common/formValidation/FormValidation";
import { validation } from "../common/formValidation/validationPatterns"
import {ValidationError} from "../common/formValidation/ValidationError";


class Registration extends React.Component {

    constructor(props){
        super(props);
        this.state = {
            username: '',
            password: '',
            confirm_password: '',
            loading:false,
            error:''
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }


    handleChange(e) {
        const { name, value } = e.target;
        this.setState({ [name]: value , error:''});
    }


    handleSubmit(object) {
       return registrationService
                .singup(object)
                    .then(() => {
                        this.props.history.push('/login');
                    })
    }

    render() {
        const {loading,error, username, password, confirm_password, submitted} = this.state;

        return (
            <div className="registration-page">
                <div className="wrapper fadeInDown">
                    <div id="formContent">

                        <div className="fadeIn first">
                            <h3>Sing up</h3>
                        </div>
                        <FormValidation
                            onSubmitFn={this.handleSubmit}
                            validation={
                                        {username:{
                                            patterns:[
                                                {
                                                    pattern:validation.required,
                                                    errorMsg:"Enter login!"
                                                },
                                                {
                                                    pattern:validation.email,
                                                    errorMsg:"Invalid email format!"
                                                }
                                            ]
                                         },
                                         password:{
                                            patterns:[
                                                {
                                                    pattern:validation.required,
                                                    errorMsg:"Enter password"
                                                },
                                                {
                                                    pattern:validation.min(6),
                                                    errorMsg:"password is too short!"
                                                },
                                                {
                                                    pattern:validation.max(8),
                                                    errorMsg:"password is too long!"
                                                }
                                            ]
                                         },
                                        confirm_password:{
                                            patterns:[{
                                                pattern:function(value){
                                                    console.log(this)
                                                    if(value !== this.password){
                                                        return false;
                                                    }
                                                    return true
                                                },
                                                errorMsg:"Password is not confirmed!"
                                            }]
                                        }
                                        }
                                    }
                        render={(props)=>(
                            <form name="form" onSubmit={props.handleSubmit}>
                                <input type="text"
                                       id="login"
                                       className="fadeIn second"
                                       name="username"
                                       value={props.state.username}
                                       onChange={props.handleChange}
                                       autocomplete="off"
                                       placeholder="login" />
                                <ValidationError state = {props.state} field={"username"}/>
                                <input type="text"
                                       id="password"
                                       value={props.state.password}
                                       className="fadeIn third"
                                       onChange={props.handleChange}
                                       name="password"
                                       autocomplete="off"
                                       placeholder="password" />
                                <ValidationError state = {props.state} field={"password"}/>
                                <input type="text"
                                       id="confirm_password"
                                       value={props.state.confirm_password}
                                       className="fadeIn third"
                                       onChange={props.handleChange}
                                       name="confirm_password"
                                       autocomplete="off"
                                       placeholder="confirm password" />
                                <ValidationError state = {props.state} field={"confirm_password"}/>
                                <input type="submit"
                                       disabled={loading}
                                       className="fadeIn fourth"
                                       value="Sing Up" />
                            </form>
                        )}
                        >

                        </FormValidation>
                        {error &&
                        <div className="error-block">
                            {error}
                        </div>
                        }
                        <div id="formFooter">
                            <Link to="/login"  className="underlineHover"> Sing In</Link>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}
export { Registration };
