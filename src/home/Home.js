import React from 'react';
import './Home.css';
import { Redirect} from "react-router-dom";
import { connect } from "react-redux";
import { userAuthService } from "../common/service/user.auth.service";
import { Role } from "../common/user/user.roles";
import { langService } from "../languages/Language.service";

function Home(props) {
    const { logged, lang } = props;
    const roles = userAuthService.getUserRoles();


    const redirectTo = (to) =>{
        const locale = langService.get();
        if(locale){
            return <Redirect to={`/${locale}${to}`} />
        }else{
            return <Redirect to={`${to}`} />
        }
    }

    return (
        <div>
            {!logged &&
                redirectTo('/login')
            }

            { roles && roles.indexOf(Role.user) === 0 &&
                redirectTo('/user')
            }
            { roles && roles.indexOf(Role.director) === 0 &&
                redirectTo('/director')
            }

            { roles && roles.indexOf(Role.teacher) === 0 &&
                redirectTo('/teacher')
            }

            { roles && roles.indexOf(Role.student) === 0 &&
                redirectTo('/student')
            }
        </div>
    );
}
function mapStateToProps(state) {
    const { logged } = state.authentication;
    const { language } = state.language;
    return {
        logged,lang:language
    };
}
const connectedHome = connect(mapStateToProps)(Home);
export  { connectedHome as Home }