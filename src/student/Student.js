import React from 'react';
import './Student.css';
import { connect } from "react-redux";
import { Header } from "../header/Header";
import { StudentHeader } from "./StudentHeader";
import { User } from "../common/user/User";
import AccessControl from "../common/AccessControl";
import NoAccess from "../common/NoAccess";
import {LangRoute} from "../languages/LangRoute";


class Student extends React.Component {

    constructor(props){
        super(props);
    }

    render() {
        return (
            <AccessControl
                allowedPermissions={["STUDENT","TEACHER","DIRECTOR"]}
                renderNoAccess={ () => <NoAccess /> }
            >
                <div>
                    <Header subHeader={ StudentHeader } />
                    <LangRoute path="/:user/mypage" render={(props) => {
                        return <User mypage={true} {...props}/>}
                    } />
                </div>
            </AccessControl>
        );
    }
}


function mapStateToProps(state) {
    const { students, loading , error} = state.students;
    return {
        students, loading, error
    };
}
const connectedStudent = connect(mapStateToProps)(Student);
export  { connectedStudent as Student }