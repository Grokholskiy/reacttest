import React from 'react';
import {LangLink} from "../languages/LangLink";

function StudentHeader() {

    return (
        <div>
            <li>
                <LangLink to="/student/mypage">My page</LangLink>
            </li>
            <li>
                <LangLink to="/student/schedule">My Schedule</LangLink>
            </li>
            <li>
                <LangLink to="/student/group">My group</LangLink>
            </li>
            <li>
                <LangLink to="/student/daybook">My daybook</LangLink>
            </li>
        </div>
    )
}


export { StudentHeader };
