import React from 'react';
import { Header } from "../header/Header";
import { UserHeader } from "./UserHeader";
import { User as UserPage} from "../common/user/User";
import { Route } from  "react-router-dom";
import {LangRoute} from "../languages/LangRoute";


class User extends React.Component {

    constructor(props){
        super(props);
    }

    render() {
        return (
            <div>
                <Header subHeader={ UserHeader } />

                <LangRoute path="/:user/mypage" render={(props) => {
                    return <UserPage mypage={true} {...props}/>}
                } />
            </div>
        );
    }
}


export { User }