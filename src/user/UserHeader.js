import React from 'react';
import { Link } from "react-router-dom";
import {LangLink} from "../languages/LangLink";

function UserHeader() {

    return (
        <div>
            <li>
                <LangLink to="/user/mypage">My page</LangLink>
            </li>
        </div>
    )
}


export { UserHeader };
