import {apiUrl} from "../../../common/constants";
import {handle} from "../../../common/service/responceHendler";
import RequestOpt from "../../../common/service/requestOptionBuilder";


export  const groupService ={
    getGroup
}

function getGroup(groupId){
    return handle(
        fetch(`${apiUrl}teacher/group/${groupId}`,new RequestOpt().GET().AUTH().JSON())
    );
}