import React from "react"
import "./Group.css"
import { groupService } from "./Group.service";
import { week, maxLesson} from "../../../common/constants";
import {Calendar} from "../../../common/Calendar";

class Group extends React.Component{
    constructor(props){
        super(props)
        this.state ={
            group:'',
            loading:true,
            error:''
        }
    }

    componentDidMount() {
        const { id } = this.props.match.params;
        this.setState({loading:true});
        groupService.getGroup(id)
            .then(group => {
                const scheduleMap = {};
                group.schedules.map(day=> scheduleMap[day.day] = day);
                group.schedules = scheduleMap;

                this.setState({loading:false, group});
            }).catch(error =>{
                console.log(error);
                this.setState({loading:false, error});
        });
    }


    render(){
        const { loading, error, group } = this.state;

        if(loading) {
            return <h2>Loading...</h2>
        }else if(error){
            return <h2>ERROR {error.message ? error.message : error}</h2>
        }


        return (
            <>
                <div className="d-flex flex-column align-items-center">
                    <h1>{group.name}</h1>
                    <Calendar />
                    {week.map(day =>{
                        return <table className="table col-10 table-striped table-bordered table-hover">
                                    <thead>
                                        <tr>
                                            <th>Students</th>
                                            <th colSpan={maxLesson.length}>
                                                {day}
                                            </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <Subjects day={day} group={group} >
                                            <Students group={group} day={day.toUpperCase()} />
                                        </Subjects>
                                    </tbody>
                                </table>
                    })}
                </div>
            </>
        );
    }
}

const Students = ({group,day})=>{
    if(group.students && group.students.length > 0){
        return <>
            {group.students.map(student =>{
                return <tr>
                    <td>
                        {student.name} {student.lastName}
                    </td>
                    <StudentMark  schedules={group.schedules[day]} />
                </tr>
            })
            }
        </>
    }else{
        return null;
    }
}

const StudentMark = ({schedules}) =>{
    if(schedules){
        return <>
            {
                schedules.daySchedule.map(lesson => {
                    return <td></td>
                })
            }
            </>
    }
    return null;
}


const Subjects = ({day, group, children}) =>{
    if(group.schedules) {
        const schedule = group.schedules[day.toUpperCase()];
        if (schedule) {
            return <>
                <tr>
                    <td></td>
                    {schedule.daySchedule.map(lesson => {
                        return <td className="subject">
                            <div>{lesson.subject.name}</div>
                        </td>
                    })}
                </tr>
                {children}
            </>
        }else{
            return  <tr>
                <td colSpan="2">
                    Free Day!
                </td>
            </tr>
        }
    }else {
        return null;
    }

}

export { Group }