import  React from 'react';
import { groupsActions } from "./Groups.action";
import { teachersActions } from "../teachers/Teachers.action"
import { Button, Col, Form, Modal, Spinner}  from "react-bootstrap";
import { connect } from "react-redux";
import { groupsService } from "./Groups.service";


class AddGroup extends React.Component{
    constructor(props){
        super(props);
        const action = this.props.action;

        this.state = {
            submited:false,
            loading:false,
            saved:false
        }

        if(action === "edit"){
            const {name, teacher} = this.props.data;
            this.state.name = name;
            if( teacher ){
                this.state.teacher = teacher.id;
            }else{
                this.state.teacher = 0;
            }
        }else {
            this.state.name = "";
            this.state.teacher = 0;
        }

        this.handleChange = this.handleChange.bind(this);
        this.closeModal = this.closeModal.bind(this);
        this.save = this.save.bind(this);
    }

    handleChange(e) {
        const { name, value } = e.target;
        this.setState({[name]: value});
    }

    closeModal(){
        const { onClose, action, location } = this.props;
        if(action === "add"){
            const backPath = location.pathname.replace("/add","");
            onClose();
            this.props.history.push(backPath );
        }else if(action === "edit"){
            onClose();
        }
    }

    save(e){
        e.preventDefault();
        this.setState({submited:true})

        if(e.target.checkValidity()){
            const {name, teacher} = this.state;
            const { dispatch, action, data } = this.props;

            this.setState({loading:true});
            let promise = action === "add" ?
                groupsService.saveGroup({name, teacher:{id:teacher}}) :
                groupsService.editGroup({id:data.id, name,teacher:{id:teacher}})

            promise
                .then(
                    group =>{
                        dispatch(action === "add" ?
                            groupsActions.save(group) : groupsActions.edit(group));

                            const teacher = group.teacher;
                            const _group = Object.assign({}, group);

                            if(teacher && teacher.id > 0){
                                delete _group.teacher;
                                teacher.group = _group;
                                const oldTeacher = this.props.data.teacher;

                                if(oldTeacher && teacher.id !== oldTeacher.id){
                                    oldTeacher.group = null;
                                    dispatch(teachersActions.edit(oldTeacher));//update previous teacher group, set null
                                }
                                dispatch(teachersActions.edit(teacher)); // update new teacher set new group
                            }else if(data.teacher ){
                                const oldTeacher = Object.assign({}, data.teacher);
                                oldTeacher.group = null;
                                dispatch(teachersActions.edit(oldTeacher));//update previous teacher group, set null
                            }
                        this.setState({loading: false, saved: true});
                    },
                    error => {
                        console.log(error);
                    }
                );
        }
    }

    render(){
        const  {name, submited, saved, loading, teacher} = this.state;
        const { action, teachers } = this.props;

        return (
            <React.Fragment>
                <Modal show={true} onHide={this.closeModal}>
                    <Modal.Header>
                        <Modal.Title>{action === "edit" ? "Edit" : "Add"} Group</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        {!saved &&
                        <Form noValidate onSubmit={ this.save }  className={submited ? "was-validated":""}>
                            <Form.Row>
                                <Form.Group as={Col}>
                                    <Form.Label>Name</Form.Label>
                                    <Form.Control
                                        type="text"
                                        name="name"
                                        value={name}
                                        onChange={this.handleChange}
                                        required
                                    />
                                    <div className="invalid-feedback">
                                        Please provide a valid name.
                                    </div>
                                </Form.Group>
                            </Form.Row>
                            <Form.Row>
                                <Form.Group as={Col}>
                                    <Form.Label>Teacher</Form.Label>
                                    <Form.Control as="select" name="teacher" onChange={this.handleChange} value={ teacher }>
                                        <option value="null" key={0}>Select teacher</option>
                                        {teachers &&
                                            teachers.map((_teacher) => {
                                                if(teacher > 0 && teacher === _teacher.id){
                                                    return <option value={_teacher.id} key={_teacher.id} >{_teacher.name} {_teacher.lastName}</option>
                                                }else if(!_teacher.group)
                                                    return <option value={_teacher.id} key={_teacher.id} >{_teacher.name} {_teacher.lastName}</option>
                                                return null;
                                            })
                                        }
                                    </Form.Control>
                                    <div className="invalid-feedback">
                                        Please provide a valid last name.
                                    </div>
                                </Form.Group>
                            </Form.Row>
                            <Modal.Footer>
                                <Button variant="secondary" onClick={this.closeModal}>
                                    Close
                                </Button>
                                {loading &&
                                <Button variant="primary" type="submit" disabled>
                                    Saving <Spinner animation="border" size="sm"/>
                                </Button>
                                }

                                {!loading &&
                                <Button variant="primary" type="submit">
                                    Save
                                </Button>
                                }
                            </Modal.Footer>
                        </Form>
                        }
                        {saved &&
                        <React.Fragment>
                            <h4>Group {name} {action === "edit" ? "Edited" : "Added"} </h4>
                            <Modal.Footer>
                                <Button variant="secondary" onClick={this.closeModal}>
                                    Close
                                </Button>
                            </Modal.Footer>
                        </React.Fragment>
                        }
                    </Modal.Body>
                </Modal>
            </React.Fragment>
        )
    }
}

function mapStateToProps(state) {
    const { teachers } = state.teachers;
    return {
        teachers
    };
}

const connectedTeachers = connect(mapStateToProps)(AddGroup);
export  { connectedTeachers as AddGroup }
