import React from 'react';
import { groupsActions } from './Groups.action';
import { groupsService } from './Groups.service';
import { connect } from "react-redux";
import  Pagination from  '../../common/Pagination'
import { AddGroup } from './Group.add';
import { Table } from "react-bootstrap";
import { DeleteConfirm } from '../../common/modal/DeleteConfirm';
import { teachersActions } from "../teachers/Teachers.action";
import FormattedMessage from "react-intl/lib/components/message";
import {LangRoute} from "../../languages/LangRoute";
import {Loading} from "../../common/loading/Loading";
import {Error} from "../../common/error/Error";
import {LangLink} from "../../languages/LangLink";

class Groups extends React.Component {

    constructor(props){
        super(props);
        this.state = {
            offset:0,
            pageLimit:10,
            edit:false,
            add:false,
            editedGroup:{},
            deletedGroup:{}
        }

        this.closeModal = this.closeModal.bind(this);
        this.showEditModal = this.showEditModal.bind(this);
        this.deleteGroup = this.deleteGroup.bind(this);
    }

    showEditModal = (group) => {
        this.setState({editedGroup: group, add:false, edit:true});
    }

    showDeleteModal = (deletedGroup, show) => {
        this.setState({_delete:show, deletedGroup});
    }

    deleteGroup(groupId){
        groupsService.deleteGroup(groupId)
        .then(() => {
            const { dispatch } = this.props;
            const { deletedGroup } = this.state;
            dispatch(groupsActions._delete(groupId));

            if(deletedGroup.teacher){
                deletedGroup.teacher.group = null;
                dispatch(teachersActions.edit(deletedGroup.teacher));
            }

            this.closeModal();
        }).catch(error => {
            console.log(error);
        });
    }

    closeModal = () => {
        this.setState({edit:false, add:false, _delete:false, editedGroup:{},deletedGroup:{}});
    }

    onPageChanged = data => {
        const { currentPage, totalPages, pageLimit } = data;
        const offset = (currentPage - 1) * pageLimit;
        this.setState({ currentPage, totalPages, offset });
    }

    componentDidMount(){
    }

    render() {
        const {loading, error, groups,location} = this.props;
        const { pageLimit, offset,edit, editedGroup, _delete, deletedGroup} = this.state;


        if(loading){
            return  <Loading />
        }else if(error){
            return <Error error={error} />
        }

        return (
            <div className="d-flex flex-column align-items-center">
                <h1>
                    <FormattedMessage id="director_group_title" ></FormattedMessage>
                </h1>
                    <Table striped bordered hover size="sm" className="col-sm-11 col-md-10 col-xl-8">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>Name</th>
                            <th>Teacher</th>
                            <th width="100px"></th>
                        </tr>
                        </thead>
                        <tbody>
                        {groups &&
                        groups.slice(offset, offset + pageLimit).map((group) => {
                            return <tr key={group.id}>
                                <td>{group.id}</td>
                                <td>
                                    <LangLink to={`/director/group/${group.id}`}>{group.name}</LangLink>
                                </td>
                                <td>
                                    {group.teacher &&
                                    <React.Fragment>
                                        {group.teacher.name} {group.teacher.lastName}
                                    </React.Fragment>
                                    }
                                </td>
                                <td className="action">
                                    <div className="editBtn" onClick={() => {this.showEditModal(group)}} title="Edit"></div>
                                    <div className="delBtn" onClick={() => {this.showDeleteModal(group, true)}} title="Delete"></div>
                                </td>
                            </tr>
                        })
                        }
                        </tbody>
                    </Table>
                    {groups.length > 10 &&
                        <div className="d-flex flex-row py-4 justify-content-center">
                            <Pagination totalRecords={groups.length} pageLimit={10} pageNeighbours={1} onPageChanged={this.onPageChanged} />
                        </div>
                    }

                    {edit &&
                        <AddGroup action="edit" data={editedGroup} onClose={this.closeModal}/>
                    }

                    {_delete &&
                      <DeleteConfirm title={`Confirm deleting ${deletedGroup.name}`}
                                   deleteObj={deletedGroup}
                                   confirmAction={this.deleteGroup}
                                   cancelAction={this.closeModal} />
                    }

                    <LangRoute path="/director/groups/add" render={(props) => <AddGroup {...props} action="add" data={{}} onClose={this.closeModal}/> } />
            </div>
        );
    }
}


function mapStateToProps(state) {
    const { groups, loading , error} = state.groups;
    const { teachers } = state.teachers;
    return {
        teachers, groups, loading, error
    };
}
const connectedTeachers = connect(mapStateToProps)(Groups);
export  { connectedTeachers as Groups }