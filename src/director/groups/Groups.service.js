import  {apiUrl} from "../../common/constants";
import {handle, ResponseType} from "../../common/service/responceHendler";
import RequestOpt from "../../common/service/requestOptionBuilder";


export const groupsService = {
    getAllGroups,
    saveGroup,
    editGroup,
    deleteGroup
};

function getAllGroups() {
    return handle(
        fetch(`${apiUrl}director/groups`,new RequestOpt().GET().AUTH())
    );
}

function saveGroup(group){
    return handle(
        fetch(`${apiUrl}director/group/add`, new RequestOpt().PUT().AUTH().JSON().BODY(group))
    );
}

function editGroup(group){
    return handle(
        fetch(`${apiUrl}director/group/edit`, new RequestOpt().PATCH().AUTH().JSON().BODY(group))
    );
}

function deleteGroup(id){
    return handle(
        fetch(`${apiUrl}director/group/delete/${id}`, new RequestOpt().DELETE().AUTH()),
        {responseType:ResponseType.VOID}
    );
}