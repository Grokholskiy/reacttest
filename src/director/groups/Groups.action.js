import { groupsService } from "./Groups.service"
import { groupsConstants } from "./Groups.constants";

export const groupsActions = {
    getAll,
    save,
    edit,
    _delete

};

function getAll() {
    return dispatch => {
        dispatch(request({ loading: true }));
        groupsService.getAllGroups()
            .then(
                groups =>{
                    dispatch(success(groups));
                },
                error => {
                    if(error.message)
                        error = error.message;
                    dispatch(failure(error));
                }
            );
    };

    function request(loading) { return { type: groupsConstants.GROUPS_LOADING, loading } }
    function success(groups) { return { type: groupsConstants.GROUPS_LOADING_SUCCESS, groups } }
    function failure(error) { return { type: groupsConstants.GROUPS_LOADING_FAILURE, error } }
}


function save(group){
    return {type: groupsConstants.GROUPS_SAVED_NEW, group}
}

function edit(group){
    return {type: groupsConstants.GROUPS_SAVE_EDIT, group}
}

function _delete(groupId){
    return {type: groupsConstants.GROUPS_DELETE, groupId}
}