import  { groupsConstants } from './Groups.constants';


const initialState = {loading: false, groups:[], error:'', edit:false, add:false}

export function GroupsReducer(state = initialState, action) {
    switch (action.type) {
        case groupsConstants.GROUPS_LOADING:
            return {
                error: '',
                loading: true,
                groups:[]
            };
        case groupsConstants.GROUPS_LOADING_SUCCESS:
            return {
                error: '',
                loading: false,
                groups: action.groups
            };
        case groupsConstants.GROUPS_LOADING_FAILURE:
            return {
                error: action.error,
                loading: false,
                groups: []
            }
        case groupsConstants.GROUPS_SAVED_NEW:
            return {
                error: '',
                loading: false,
                groups: [...state.groups, action.group]
            }
        case groupsConstants.GROUPS_ADD_NEW:
            return {
                ...state,
                edit:false,
                add:true
            }
        case groupsConstants.GROUPS_SAVE_EDIT:
            return {
                ...state,
                groups: state.groups.map((group) => group.id === action.group.id ? action.group : group)
            }
        case groupsConstants.GROUPS_DELETE:
            return {
                ...state,
                groups: state.groups.filter((group) => group.id !== action.groupId)
            }

        default:
            return state
    }
}




