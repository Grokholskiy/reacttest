import React from 'react';
import { Header } from "../header/Header";
import { Route } from "react-router-dom";
import { DirectorHeader } from "./DirectorHeader";
import { teachersActions } from "./teachers/Teachers.action";
import { groupsActions } from "./groups/Groups.action";
import {connect} from "react-redux";
import ErrorBoundary from '../common/ErrorBoundary';
import { Students } from "./students/Students";
import { Teachers } from './teachers/Teachers';

import { Groups } from "./groups/Groups";
import { Subjects } from "./subjects/Subjects";
import { User} from "../common/user/User";
import { Schedule } from "../schedule/Schedules"
import {subjectsActions} from "./subjects/Subjects.action";
import AccessControl from "../common/AccessControl";
import { Group } from "./groups/group/Group";
import NoAccess from "../common/NoAccess";
import {LangRoute} from "../languages/LangRoute";


class Director extends React.Component {

    componentDidMount() {
        const { dispatch } = this.props;
        dispatch(teachersActions.getAll());
        dispatch(groupsActions.getAll());
        dispatch(subjectsActions.getAll());
    }

    render(){
        return(
            <AccessControl
                allowedPermissions={["DIRECTOR"]}
                renderNoAccess={ () => <NoAccess /> }
            >
                <div>
                    <Header subHeader={ DirectorHeader } />
                    <ErrorBoundary>
                        <LangRoute path="/director/students" component={ Students } />
                        <LangRoute path="/director/teachers" component={ Teachers } />
                        <LangRoute path="/director/groups"   component={ Groups } />
                        <LangRoute path="/director/group/:id(\d+)" component={ Group } />
                        <LangRoute path="/director/subject" component={ Subjects } />
                        <LangRoute path="/director/schedule"  component={ Schedule } />
                        <LangRoute path="/:user/mypage" render={(props) => {
                            return <User mypage={true} {...props}/>}
                        } />
                        <LangRoute path="/director/:user(student|teacher|director)/:id(\d+)" component={ User } />

                    </ErrorBoundary>
                </div>
            </AccessControl>
        )
    }
}


function mapStateToProps(state) {
    const { teachers } = state.teachers;
    return {
        teachers
    };
}
const connectedTeachers = connect(mapStateToProps)(Director);
export  { connectedTeachers as Director }