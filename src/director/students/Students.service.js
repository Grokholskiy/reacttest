import  {apiUrl} from "../../common/constants";
import {handle, ResponseType} from "../../common/service/responceHendler";
import RequestOpt from "../../common/service/requestOptionBuilder";

export const studentsService = {
    getAllStudents,
    saveStudent,
    editStudent,
    deleteStudent
};

function getAllStudents() {
    return handle(
        fetch(`${apiUrl}student/students`, new RequestOpt().GET().AUTH())
    );
}

function saveStudent(student){
    return handle(
        fetch(`${apiUrl}director/student/add`, new RequestOpt().PUT().AUTH().JSON().BODY(student))
    );
}

function editStudent(student){
    return handle(
        fetch(`${apiUrl}director/student/edit`, new RequestOpt().PATCH().AUTH().JSON().BODY(student))
    );
}

function deleteStudent(id){
    return handle(
        fetch(`${apiUrl}director/student/delete/${id}`, new RequestOpt().DELETE().AUTH()),
        {responseType:ResponseType.VOID}
    );
}