import { studentsService } from "./Students.service"
import { studentsConstants } from "./Students.constants";

export const studentsActions = {
    getAll,save,edit,_delete
};

function getAll() {
    return dispatch => {
        dispatch(request({ loading: true }));

        studentsService.getAllStudents()
            .then(
                students => {
                    dispatch(success(students));
                },
                error => {
                    if(error.message)
                        error = error.message
                    dispatch(failure(error));

                }
            );
    };

    function request(loading) { return { type: studentsConstants.STUDENTS_LOADING, loading } }
    function success(students) { return { type: studentsConstants.STUDENTS_LOADING_SUCCESS, students } }
    function failure(error) { return { type: studentsConstants.STUDENTS_LOADING_FAILURE, error } }
}


function save(student){
    return {type: studentsConstants.STUDENTS_SAVED_NEW, student}
}

function edit(student){
    return {type: studentsConstants.STUDENTS_SAVE_EDIT, student}
}

function _delete(studentId){
    return {type: studentsConstants.STUDENTS_DELETE, studentId}
}