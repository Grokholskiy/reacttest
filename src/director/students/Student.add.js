import  React from 'react';
import { studentsActions } from "./Students.action";
import { Button, Col, Form, Modal, Spinner}  from "react-bootstrap";
import { connect } from "react-redux";
import { studentsService } from "./Students.service";


class AddStudent extends React.Component{
    constructor(props){
        super(props);
        const action = this.props.action;

        this.state = {
            submited:false,
            loading:false,
            saved:false
        }


        if(action === "edit"){
            const {name, lastName, group} = this.props.data;

            this.state.name = name;
            this.state.lastname = lastName;
            if(group){
                this.state.group = group.id;
            }else{
                this.state.group = 0;
            }
        }else{
            this.state.name = "";
            this.state.lastname = "";
            this.state.group = 0;
        }

        this.handleChange = this.handleChange.bind(this);
        this.closeModal = this.closeModal.bind(this);
        this.save = this.save.bind(this);
    }

    handleChange(e) {
        const { name, value } = e.target;
        this.setState({[name]: value})
    }

    closeModal(){
        const { onClose, action, location } = this.props;

        if(action === "add"){
            const backPath = location.pathname.replace("/add", "");
            onClose();
            this.props.history.push(backPath);

        }else if(action === "edit"){
            onClose();
        }
    }

    save(e){
        e.preventDefault();
        this.setState({submited:true})

        if(e.target.checkValidity()){
            const {name, lastname, group} = this.state;
            const { dispatch, action, data } = this.props;

            this.setState({loading:true});

            let promise = action === "add" ?
                studentsService.saveStudent({name, lastName:lastname, group:{id:group}}) :
                studentsService.editStudent({id:data.id, name,lastName:lastname, group:{id:group}})

            promise
                .then(
                    teachers =>{
                        dispatch(action === "add" ?
                            studentsActions.save(teachers) :
                            studentsActions.edit(teachers));

                        this.setState({loading: false, saved: true});
                    },
                    error => {
                        console.log(error);
                    }
                );
        }
    }

    render(){
        const  {name, lastname, group, submited, saved, loading} = this.state;
        const { action, groups } = this.props;
        return (
            <React.Fragment>
                <Modal show={true} onHide={this.closeModal}>
                    <Modal.Header>
                        <Modal.Title>{action === "edit" ? "Edit" : "Add"} Student</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        {!saved &&
                        <Form noValidate onSubmit={this.save}  className={submited ? "was-validated":""}>
                            <Form.Row>
                                <Form.Group as={Col}>
                                    <Form.Label>Name</Form.Label>
                                    <Form.Control
                                        type="text"
                                        name="name"
                                        value={name}
                                        onChange={this.handleChange}
                                        required
                                    />
                                    <div className="invalid-feedback">
                                        Please provide a valid name.
                                    </div>
                                </Form.Group>
                            </Form.Row>
                            <Form.Row>
                                <Form.Group as={Col}>
                                    <Form.Label>Last Name</Form.Label>
                                    <Form.Control
                                        type="text"
                                        name="lastname"
                                        value={lastname}
                                        onChange={this.handleChange}
                                        required
                                    />
                                    <div className="invalid-feedback">
                                        Please provide a valid last name.
                                    </div>
                                </Form.Group>
                            </Form.Row>
                            <Form.Row>
                                <Form.Group as={Col}>
                                    <Form.Label>Group</Form.Label>
                                    <Form.Control as="select" name="group" onChange={this.handleChange} value={ group }>
                                        <option value="0" key={0}>Select group</option>
                                            {groups &&
                                                groups.map((group) => {
                                                    return <option value={group.id} key={group.id} >{group.name} </option>
                                                })
                                            }
                                    </Form.Control>
                                    <div className="invalid-feedback">
                                        Please provide a valid group
                                    </div>
                                </Form.Group>
                            </Form.Row>
                            <Modal.Footer>
                                <Button variant="secondary" onClick={this.closeModal}>
                                    Close
                                </Button>
                                {loading &&
                                <Button variant="primary" type="submit" disabled>
                                    Saving <Spinner animation="border" size="sm"/>
                                </Button>
                                }

                                {!loading &&
                                <Button variant="primary" type="submit">
                                    Save
                                </Button>
                                }
                            </Modal.Footer>
                        </Form>
                        }
                        {saved &&
                        <React.Fragment>
                            <h4>Student {name} {lastname} {action === "edit" ? "Edited" : "Added"} </h4>
                            <Modal.Footer>
                                <Button variant="secondary" onClick={this.closeModal}>
                                    Close
                                </Button>
                            </Modal.Footer>
                        </React.Fragment>
                        }
                    </Modal.Body>
                </Modal>
            </React.Fragment>
        )
    }
}

function mapStateToProps(state) {
    const { groups } = state.groups;
    return {
        groups
    };
}

const connectedStudent = connect(mapStateToProps)(AddStudent);
export  { connectedStudent as AddStudent }
