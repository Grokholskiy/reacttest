import  { studentsConstants } from './Students.constants';

const initialState = {loading: false, students:[], error:''}

export function StudentsReducer(state = initialState, action) {
    switch (action.type) {
        case studentsConstants.STUDENTS_LOADING:
            return {
                error: '',
                loading: true,
                students:[]
            };
        case studentsConstants.STUDENTS_LOADING_SUCCESS:
            return {
                error: '',
                loading: false,
                students: action.students
            };
        case studentsConstants.STUDENTS_LOADING_FAILURE:
            return {
                error: action.error,
                loading: false,
                students: []
            }
        case studentsConstants.STUDENTS_SAVED_NEW:
            return {
                error: '',
                loading: false,
                students: [...state.students, action.student]
            }
        case studentsConstants.STUDENTS_ADD_NEW:
            return {
                ...state,
                edit:false,
                add:true
            }
        case studentsConstants.STUDENTS_SAVE_EDIT:
            return {
                ...state,
                students: state.students.map((student, index) => student.id === action.student.id ? action.student : student)
            }
        case studentsConstants.STUDENTS_DELETE:
            return {
                ...state,
                students: state.students.filter((student) => student.id !== action.studentId)
            }
        default:
            return state
    }
}