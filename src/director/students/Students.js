import React from 'react';
import './Students.css';
import { studentsActions } from './Students.action';
import {connect} from "react-redux";
import Pagination from "../../common/Pagination";
import {Table} from "react-bootstrap";
import { studentsService } from "./Students.service";
import { AddStudent } from "./Student.add";
import {DeleteConfirm} from "../../common/modal/DeleteConfirm";
import {Loading} from "../../common/loading/Loading";
import {Error} from "../../common/error/Error";
import {LangRoute} from "../../languages/LangRoute";
import {LangLink} from "../../languages/LangLink";


class Students extends React.Component {

    constructor(props){
        super(props);
        this.state = {
            offset:0,
            pageLimit:10,
            edit:false,
            add:false,
            editedStudent:{},
            deletedStudent:{}
        }

        this.closeModal = this.closeModal.bind(this);
        this.showEditModal = this.showEditModal.bind(this);
        this.deleteStudent= this.deleteStudent.bind(this);
    }

    showEditModal = (student) => {
        this.setState({editedStudent: student,add:false, edit:true});
    }

    showDeleteModal = (deletedStudent, show) => {
        this.setState({_delete:show, deletedStudent});
    }

    deleteStudent(studentId){
        studentsService.deleteStudent(studentId)
        .then(() => {
            const { dispatch } = this.props;
            dispatch(studentsActions._delete(studentId));
            this.closeModal();
        }).catch(error => {
            console.log(error);
        });
    }

    closeModal = () => {
        this.setState({edit:false, add:false, _delete:false, editedStudent:{},deletedStudent:{}});
    }

    onPageChanged = data => {
        const { currentPage, totalPages, pageLimit } = data;
        const offset = (currentPage - 1) * pageLimit;
        this.setState({ currentPage, totalPages, offset });
    }

    componentDidMount(){
        const { dispatch } = this.props;
        dispatch(studentsActions.getAll());
    }

    render() {
        const {loading, error, students} = this.props;
        const { pageLimit, offset,edit, editedStudent, _delete, deletedStudent} = this.state;

        if(loading){
            return  <Loading />
        }else if(error){
            return <Error error={error} />
        }

        return (
            <div className="d-flex flex-column align-items-center">
                <h1>Students Page</h1>
                    <Table striped bordered hover size="sm" className="col-sm-11 col-md-10 col-xl-8">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>Name</th>
                            <th>Last name</th>
                            <th>Group</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        {students &&
                        students.slice(offset, offset + pageLimit).map((student) => {
                            return <tr key={student.id}>
                                <td>{student.id}</td>
                                <td>
                                    <LangLink to={`/director/student/${student.id}`}>{student.name}</LangLink>
                                </td>
                                <td>
                                    <LangLink to={`/director/student/${student.id}`}>{student.lastName}</LangLink>
                                </td>
                                <td>{student.group && student.group.name}</td>
                                <td className="action">
                                    <div className="editBtn" onClick={() => {this.showEditModal(student)}} title="Edit"></div>
                                    <div className="delBtn" onClick={() => {this.showDeleteModal(student, true)}} title="Delete"></div>
                                </td>
                            </tr>
                        })
                        }
                        </tbody>
                    </Table>
                    {students.length > 10 &&
                    <div className="d-flex flex-row py-4 justify-content-center">
                        <Pagination totalRecords={students.length} pageLimit={10} pageNeighbours={1} onPageChanged={this.onPageChanged} />
                    </div>
                    }

                    {edit &&
                    <AddStudent action="edit" data={editedStudent} onClose={this.closeModal}/>
                    }

                    {_delete &&
                    <DeleteConfirm title={`Confirm deleting ${deletedStudent.name} ${deletedStudent.lastName}`}
                                   deleteObj={deletedStudent}
                                   confirmAction={this.deleteStudent}
                                   cancelAction={this.closeModal} />
                    }

                    <LangRoute path="/director/students/add" render={(props) => <AddStudent {...props} action="add" data={{}} onClose={this.closeModal}/> } />
            </div>
        );
    }
}


function mapStateToProps(state) {
    const { students, loading , error} = state.students;
    return {
        students, loading, error
    };
}
const connectedStudents = connect(mapStateToProps)(Students);
export  { connectedStudents as Students }