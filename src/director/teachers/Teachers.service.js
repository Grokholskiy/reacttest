import  {apiUrl} from "../../common/constants";
import {handle, ResponseType} from "../../common/service/responceHendler"
import RequestOpt from "../../common/service/requestOptionBuilder"

export const teachersService = {
    getAllTeacher,
    saveTeacher,
    editTeacher,
    deleteTeacher
};

function getAllTeacher() {
    return handle(
        fetch(`${apiUrl}teacher/teachers`,new RequestOpt().GET().AUTH())
    );
}

function saveTeacher(teacher){
    return handle(
        fetch(`${apiUrl}director/teacher/add`, new RequestOpt().PUT().AUTH().JSON().BODY(teacher))
    );
}

function editTeacher(teacher){
    return handle(
        fetch(`${apiUrl}director/teacher/edit`, new RequestOpt().PATCH().AUTH().JSON().BODY(teacher))
    );
}

function deleteTeacher(id){
    return handle(
        fetch(`${apiUrl}director/teacher/delete/${id}`, new RequestOpt().DELETE().AUTH()),
        {responseType:ResponseType.VOID}
    );
}