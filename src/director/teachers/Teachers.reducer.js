import  { teachersConstants } from './Teachers.constants';

const initialState = {loading: false, teachers:[], error:'', edit:false, add:false}

export function TeachersReducer(state = initialState, action) {
    switch (action.type) {
        case teachersConstants.TEACHERS_LOADING:
            return {
                error: '',
                loading: true,
                teachers:[]
            };
        case teachersConstants.TEACHERS_LOADING_SUCCESS:
            return {
                error: '',
                loading: false,
                teachers: action.teachers
            };
        case teachersConstants.TEACHERS_LOADING_FAILURE:
            return {
                error: action.error,
                loading: false,
                teachers: []
            }
        case teachersConstants.TEACHER_SAVED_NEW:
            return {
                error: '',
                loading: false,
                teachers: [...state.teachers, action.teacher]
            }
        case teachersConstants.TEACHER_ADD_NEW:
            return {
                ...state,
                edit:false,
                add:true
            }
        case teachersConstants.TEACHER_SAVE_EDIT:
            return {
                ...state,
                teachers: state.teachers.map(
                    (teacher) => teacher.id === action.teacher.id 
                        ? {...teacher, ...action.teacher}
                        : teacher)
            }
        case teachersConstants.TEACHER_SUBJECT_DELETE:
        return {
            ...state,
               teachers: state.teachers.map(
                    (teacher) => teacher.id === action.teacher.id 
                        ? {...teacher,subjects: teacher.subjects.filter(subject=> subject.id !== action.teacher.subject.id ) }
                        : teacher)
        }
        case teachersConstants.TEACHER_SUBJECT_ADD:
        return {
            ...state,
               teachers: state.teachers.map(
                    (teacher) => teacher.id === action.teacher.id 
                        ? {...teacher,subjects: [...teacher.subjects,action.teacher.subject ] }
                        : teacher)
        }
        case teachersConstants.TEACHER_DELETE:
            return {
                ...state,
                teachers: state.teachers.filter((teacher) => teacher.id !== action.teacherId)
            }
        default:
            return state
    }
}




