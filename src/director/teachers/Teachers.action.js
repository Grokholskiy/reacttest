import { teachersService } from "./Teachers.service"
import { teachersConstants } from "./Teachers.constants";

export const teachersActions = {
    getAll,
    save,
    edit,
    _delete,
    deleteSubject,
    addSubject
};

function getAll() {
    return dispatch => {
        dispatch(request({ loading: true }));

        teachersService.getAllTeacher()
            .then(
            teachers =>{
                dispatch(success(teachers));
            },
            error => {
                if(error.message)
                    error = error.message;
                dispatch(failure(error));

            }
        );
    };

    function request(loading) { return { type: teachersConstants.TEACHERS_LOADING, loading } }
    function success(teachers) { return { type: teachersConstants.TEACHERS_LOADING_SUCCESS, teachers } }
    function failure(error) { return { type: teachersConstants.TEACHERS_LOADING_FAILURE, error } }
}



function save(teacher){
    return {type: teachersConstants.TEACHER_SAVED_NEW, teacher}
}

function edit(teacher){
    return {type: teachersConstants.TEACHER_SAVE_EDIT, teacher}
}

function _delete(teacherId){
    return {type: teachersConstants.TEACHER_DELETE, teacherId}
}

function deleteSubject(teacher){
    return {type: teachersConstants.TEACHER_SUBJECT_DELETE, teacher}
}

function addSubject(teacher){
    return {type: teachersConstants.TEACHER_SUBJECT_ADD, teacher}
}
