import  React from 'react';
import { teachersActions } from "./Teachers.action";
import { Button, Col, Form, Modal, Spinner}  from "react-bootstrap";
import { connect } from "react-redux";
import { teachersService } from "./Teachers.service";
import {groupsActions} from "../groups/Groups.action";


class TeacherAdd extends React.Component{
    constructor(props){
        super(props);
        const action = this.props.action;

        this.state = {
            submited:false,
            loading:false,
            saved:false
        }

        if(action === "edit"){
            const {name, lastName} = this.props.data;
            this.state.name = name;
            this.state.lastname = lastName;
        }else{
            this.state.name = "";
            this.state.lastname = "";
        }

        this.handleChange = this.handleChange.bind(this);
        this.closeModal = this.closeModal.bind(this);
        this.save = this.save.bind(this);
    }

    handleChange(e) {
        const { name, value } = e.target;
        this.setState({[name]: value})
    }

    closeModal(){
        const { onClose, action, location } = this.props;
        if(action === "add"){
            const  backPath = location.pathname.replace("/add", "");
            onClose();
            this.props.history.push(backPath);

        }else if(action === "edit"){
            onClose();
        }
    }

    save(e){
        e.preventDefault();
        this.setState({submited:true})

        if(e.target.checkValidity()){
            const {name, lastname} = this.state;
            const { dispatch, action, data } = this.props;

            this.setState({loading:true});

            let promise = action === "add" ?
                teachersService.saveTeacher({name, lastName:lastname}) :
                teachersService.editTeacher({id:data.id, name,lastName:lastname})

            promise
                .then(
                    teacher =>{
                        dispatch(action === "add" ?
                            teachersActions.save(teacher) :
                            teachersActions.edit(teacher));

                        const oldGroup = data.group;

                        if( oldGroup ){
                            const newTeacher = Object.assign({}, teacher );
                            oldGroup.teacher = newTeacher;
                            dispatch(groupsActions.edit(oldGroup));
                        }
                        this.setState({loading: false, saved: true});
                    },
                    error => {
                        console.log(error);
                    }
                );
        }
    }

    render(){
        const  {name, lastname, submited, saved, loading} = this.state;
        const { action } = this.props;
        return (
            <React.Fragment>
                <Modal show={true} onHide={this.closeModal}>
                    <Modal.Header>
                        <Modal.Title>{action === "edit" ? "Edit" : "Add"} Teacher</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        {!saved &&
                        <Form noValidate onSubmit={this.save}  className={submited ? "was-validated":""}>
                            <Form.Row>
                                <Form.Group as={Col}>
                                    <Form.Label>Name</Form.Label>
                                    <Form.Control
                                        type="text"
                                        name="name"
                                        value={name}
                                        onChange={this.handleChange}
                                        required
                                    />
                                    <div className="invalid-feedback">
                                        Please provide a valid name.
                                    </div>
                                </Form.Group>
                            </Form.Row>
                            <Form.Row>
                                <Form.Group as={Col}>
                                    <Form.Label>Last Name</Form.Label>
                                    <Form.Control
                                        type="text"
                                        name="lastname"
                                        value={lastname}
                                        onChange={this.handleChange}
                                        required
                                    />
                                    <div className="invalid-feedback">
                                        Please provide a valid last name.
                                    </div>
                                </Form.Group>
                            </Form.Row>
                            <Modal.Footer>
                                <Button variant="secondary" onClick={this.closeModal}>
                                    Close
                                </Button>
                                {loading &&
                                    <Button variant="primary" type="submit" disabled>
                                        Saving <Spinner animation="border" size="sm"/>
                                    </Button>
                                }

                                {!loading &&
                                    <Button variant="primary" type="submit">
                                        Save
                                    </Button>
                                }
                            </Modal.Footer>
                        </Form>
                    }
                    {saved &&
                        <React.Fragment>
                            <h4>Teacher {name} {lastname} {action === "edit" ? "Edited" : "Added"} </h4>
                             <Modal.Footer>
                                <Button variant="secondary" onClick={this.closeModal}>
                                     Close
                                </Button>
                            </Modal.Footer>
                        </React.Fragment>
                    }
                    </Modal.Body>
                </Modal>
            </React.Fragment>
        )
    }
}

function mapStateToProps(state) {
    return {};
}

const connectedTeachers = connect(mapStateToProps)(TeacherAdd);
export  { connectedTeachers as TeacherAdd }
