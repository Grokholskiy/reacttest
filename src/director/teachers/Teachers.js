import React from 'react';
import './Teachers.css';
import { teachersActions } from './Teachers.action';
import { teachersService } from './Teachers.service';
import { connect } from "react-redux";
import  Pagination from  '../../common/Pagination'
import { Route, Link } from "react-router-dom";
import { TeacherAdd } from './Teacher.add';
import { Table } from "react-bootstrap";
import { DeleteConfirm } from '../../common/modal/DeleteConfirm';
import { groupsActions } from "../groups/Groups.action";
import Popover from "react-bootstrap/Popover";
import Button from "react-bootstrap/Button";
import OverlayTrigger from "react-bootstrap/es/OverlayTrigger";
import {LangRoute} from "../../languages/LangRoute";
import {Loading} from "../../common/loading/Loading";
import {Error} from "../../common/error/Error";



class Teachers extends React.Component {

    constructor(props){
        super(props);
        this.state = {
            offset:0,
            pageLimit:10,
            edit:false,
            add:false,
            editedTeacher:{},
            deletedTeacher:{}
        }

        this.closeModal = this.closeModal.bind(this);
        this.showEditModal = this.showEditModal.bind(this);
        this.deleteTeacher = this.deleteTeacher.bind(this);
    }

    showEditModal = (teacher) => {
        this.setState({editedTeacher: teacher,add:false, edit:true});
    }

    showDeleteModal = (deletedTeacher, show) => {
        this.setState({_delete:show, deletedTeacher});
    }

    deleteTeacher(teacherId){
        teachersService.deleteTeacher(teacherId)
            .then(() => {
                const { dispatch } = this.props;
                const { deletedTeacher } = this.state;

                dispatch(teachersActions._delete(teacherId));

                if(deletedTeacher.group){
                    deletedTeacher.group.teacher = null;
                    dispatch(groupsActions.edit(deletedTeacher.group))
                }

                this.closeModal();
            }).catch(error => {
                console.log(error);
            });
    }

    closeModal = () => {
        this.setState({edit:false, add:false, _delete:false, editedTeacher:{},deletedTeacher:{}});
    }

    onPageChanged = data => {
        const { currentPage, totalPages, pageLimit } = data;
        const offset = (currentPage - 1) * pageLimit;
        this.setState({ currentPage, totalPages, offset });
    }

    componentDidMount(){
        // const { dispatch } = this.props;
        // dispatch(teachersActions.getAll())
    }

    render() {
        const {loading, error, teachers} = this.props;
        const { pageLimit, offset,edit, editedTeacher, _delete, deletedTeacher} = this.state;

        if(loading){
            return  <Loading />
        }else if(error){
            return <Error error={error} />
        }

        return (
            <div className="d-flex flex-column align-items-center">
                <h1>Teachers Page</h1>
                    <Table striped bordered hover size="sm" className="col-sm-11 col-md-10 col-xl-8">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>Name</th>
                            <th>Last name</th>
                            <th>Group</th>
                            <th>Subjects</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        {teachers &&
                        teachers.slice(offset, offset + pageLimit).map((teacher) => {
                            return <tr key={teacher.id}>
                                <td>{teacher.id}</td>
                                <td>
                                    <Link to={`/director/teacher/${teacher.id}`}>{teacher.name}</Link>
                                </td>
                                <td>
                                    <Link to={`/director/teacher/${teacher.id}`}>{teacher.lastName}</Link>
                                </td>
                                <td>
                                    {teacher.group &&
                                        teacher.group.name
                                    }
                                    {!teacher.group &&
                                        <>-</>
                                    }
                                </td>
                                <td>
                                    {teacher.subjects && teacher.subjects.length > 0 &&
                                        <OverlayTrigger trigger="click" placement="right" overlay={
                                            <Popover id="popover-basic" title={`${teacher.name}'s subjects`}>
                                                <ul>
                                                    {
                                                        teacher.subjects.map((subject) =>{
                                                            return <li key={subject.id}>{subject.name}</li>
                                                        })
                                                    }
                                                </ul>
                                            </Popover>
                                        }>
                                            <Button variant="link" size="sm">Subjects</Button>
                                        </OverlayTrigger>
                                    }

                                    {(!teacher.subjects || teacher.subjects.length === 0) &&
                                        <>-</>
                                    }
                                </td>
                                <td className="action">
                                    <div className="editBtn" onClick={() => {this.showEditModal(teacher)}} title="Edit"></div>
                                    <div className="delBtn" onClick={() => {this.showDeleteModal(teacher, true)}} title="Delete"></div>
                                </td>
                            </tr>
                        })
                        }
                        </tbody>
                    </Table>
                    {teachers.length > 10 &&
                    <div className="d-flex flex-row py-4 justify-content-center">
                        <Pagination totalRecords={teachers.length} pageLimit={10} pageNeighbours={1} onPageChanged={this.onPageChanged} />
                    </div>
                    }

                    {edit &&
                    <TeacherAdd action="edit" data={editedTeacher} onClose={this.closeModal}/>
                    }

                    {_delete &&
                    <DeleteConfirm title={`Confirm deleting ${deletedTeacher.name} ${deletedTeacher.lastName}`}
                                   deleteObj={deletedTeacher}
                                   confirmAction={this.deleteTeacher}
                                   cancelAction={this.closeModal} />
                    }

                    <LangRoute path="/director/teachers/add" render={(props) => <TeacherAdd {...props} action="add" data={{}} onClose={this.closeModal}/> } />
            </div>
        );
    }
}


function mapStateToProps(state) {
    const { teachers, loading , error} = state.teachers;
    return {
        teachers, loading, error
    };
}
const connectedTeachers = connect(mapStateToProps)(Teachers);
export  { connectedTeachers as Teachers }
