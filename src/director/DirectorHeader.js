import React from 'react';
import { LangLink } from "../languages/LangLink";
import FormattedMessage from "react-intl/lib/components/message";


function DirectorHeader() {

    return (
        <React.Fragment>
            <li>
                <LangLink to="/director/mypage">
                    <FormattedMessage id="director.director"></FormattedMessage>
                </LangLink>
            </li>
            <li>
                <LangLink to="/director/teachers" activeClassName='active'>Teachers</LangLink>
                <ul>
                    <li>
                        <LangLink to="/director/teachers/add">Add Teacher</LangLink>
                    </li>
                </ul>
            </li>
            <li>
                <LangLink to="/director/students" activeClassName='active'>
                    <FormattedMessage id="director.students"></FormattedMessage>
                </LangLink>
                <ul>
                    <li>
                        <LangLink to="/director/students/add" >Add Student</LangLink>
                    </li>
                </ul>
            </li>
            <li>
                <LangLink to="/director/groups" activeClassName='active'>
                    <FormattedMessage id="director.groups"></FormattedMessage>
                </LangLink>
                <ul>
                    <li>
                        <LangLink to="/director/groups/add">Add Group</LangLink>
                    </li>
                </ul>
            </li>
            <li>
                <LangLink to="/director/subject" activeClassName='active'>
                    <FormattedMessage id="director.subjects"></FormattedMessage>
                </LangLink>
                <ul>
                    <li>
                        <LangLink to="/director/subject/add">Add Subject</LangLink>
                    </li>
                </ul>
            </li>
            <li>
                <LangLink to="/director/schedule" >
                    <FormattedMessage id="director.schedule"></FormattedMessage>
                </LangLink>
            </li>
        </React.Fragment>
    )
}


export { DirectorHeader };
