import  {apiUrl} from "../../common/constants";
import {handle, ResponseType} from "../../common/service/responceHendler";
import RequestOpt from "../../common/service/requestOptionBuilder";

export const subjectsService = {
    getAll,
    save,
    edit,
    _delete
};

function getAll() {
    return handle(
        fetch(`${apiUrl}student/subjects`,new RequestOpt().GET().AUTH())
    );
}

function save(subject){
    return handle(
        fetch(`${apiUrl}director/subject/add`, new RequestOpt().PUT().AUTH().JSON().BODY(subject))
    );
}

function edit(subject){
    return handle(
        fetch(`${apiUrl}director/subject/edit`, new RequestOpt().PATCH().AUTH().JSON().BODY(subject))
    );
}

function _delete(id){
    return handle(
        fetch(`${apiUrl}director/subject/delete/${id}`, new RequestOpt().DELETE().AUTH()),
        {responseType:ResponseType.VOID}
    );
}