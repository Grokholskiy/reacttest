import  React from 'react';
import { subjectsActions } from "./Subjects.action";
import { Button, Col, Form, Modal, Spinner}  from "react-bootstrap";
import { connect } from "react-redux";
import { subjectsService } from "./Subjects.service";
import { teachersActions } from '../teachers/Teachers.action';


class SubjectAdd extends React.Component{
    constructor(props){
        super(props);
        const action = this.props.action;

        this.state = {
            submited:false,
            loading:false,
            saved:false
        }

        if(action === "edit"){

            const {name, teacher, group} = this.props.data;

            this.state.name = name;
            if( teacher ){
                this.state.teacher = teacher.id;
            }else{
                this.state.teacher = 0;
            }

            if( group ){
                this.state.group  = group.id;
            }else{
                this.state.group  = 0;
            }

        }else {
            this.state.name = "";
            this.state.teacher = 0;
            this.state.group = 0;
        }

        this.handleChange = this.handleChange.bind(this);
        this.closeModal = this.closeModal.bind(this);
        this.save = this.save.bind(this);
    }

    handleChange(e) {
        const { name, value } = e.target;
        this.setState({[name]: value})
    }

    closeModal(){
        const { onClose, action, location } = this.props;

        if(action === "add"){
            const backPath = location.pathname.replace("/add","");
            onClose();
            this.props.history.push(backPath);

        }else if(action === "edit"){
            onClose();
        }
    }

    save(e){
        e.preventDefault();
        this.setState({submited:true})

        if(e.target.checkValidity()){
            const { name, teacher, group } = this.state;
            const { dispatch, action, data } = this.props;

            this.setState({loading:true});

            let promise = action === "add" ?
                subjectsService.save({name, teacher:{id:teacher}, group:{id:group}}) :
                subjectsService.edit({id:data.id, name,teacher:{id:teacher}, group:{id:group}})

            promise
                .then(
                    subject =>{
                        dispatch(action === "add" ?
                            subjectsActions.save(subject) :
                            subjectsActions.edit(subject));
                            
                            const oldTeacher = data.teacher;
                            const newTeacher = {...subject.teacher, subject:{...subject} };
                            
                            if(oldTeacher){
                                dispatch(teachersActions.deleteSubject({id:oldTeacher.id, subject:{id:subject.id}}));
                            }

                            delete newTeacher.subject.teacher;
                            
                            dispatch(teachersActions.addSubject(newTeacher));
                        this.setState({loading: false, saved: true});
                    },
                    error => {
                        console.log(error);
                    }
                );
        }
    }

    render(){
        const  {name, teacher, submited, saved, loading, group} = this.state;
        const { action, teachers, groups } = this.props;
        return (
            <React.Fragment>
                <Modal show={true} onHide={this.closeModal}>
                    <Modal.Header>
                        <Modal.Title>{action === "edit" ? "Edit" : "Add"} Subject</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        {!saved &&
                        <Form noValidate onSubmit={this.save}  className={submited ? "was-validated":""}>
                            <Form.Row>
                                <Form.Group as={Col}>
                                    <Form.Label>Name</Form.Label>
                                    <Form.Control
                                        type="text"
                                        name="name"
                                        value={name}
                                        onChange={this.handleChange}
                                        required
                                    />
                                    <div className="invalid-feedback">
                                        Please provide a valid name.
                                    </div>
                                </Form.Group>
                            </Form.Row>

                            <Form.Row>
                                <Form.Group as={Col}>
                                    <Form.Label>Group</Form.Label>
                                    <Form.Control as="select" name="group" onChange={this.handleChange} value={ group }>
                                        <option value="0" key={0}>Select Group</option>
                                        {groups &&
                                            groups.map((group) => {
                                                return <option value={group.id} key={group.id} >{group.name} </option>
                                            })
                                        }
                                    </Form.Control>
                                    <div className="invalid-feedback">
                                        Please provide a valid last name.
                                    </div>
                                </Form.Group>
                            </Form.Row>

                            <Form.Row>
                                <Form.Group as={Col}>
                                    <Form.Label>Teacher</Form.Label>
                                    <Form.Control as="select" name="teacher" onChange={this.handleChange} value={ teacher }>
                                        <option value="0" key={0}>Select teacher</option>
                                        {teachers &&
                                            teachers.map((teacher) => {
                                                return <option value={teacher.id} key={teacher.id} >{teacher.name} {teacher.lastName}</option>
                                            })
                                        }
                                    </Form.Control>
                                    <div className="invalid-feedback">
                                        Please provide a valid last name.
                                    </div>
                                </Form.Group>
                            </Form.Row>
                            <Modal.Footer>
                                <Button variant="secondary" onClick={this.closeModal}>
                                    Close
                                </Button>
                                {loading &&
                                <Button variant="primary" type="submit" disabled>
                                    Saving <Spinner animation="border" size="sm"/>
                                </Button>
                                }

                                {!loading &&
                                <Button variant="primary" type="submit">
                                    Save
                                </Button>
                                }
                            </Modal.Footer>
                        </Form>
                        }
                        {saved &&
                        <React.Fragment>
                            <h4>Teacher {name}  {action === "edit" ? "Edited" : "Added"} </h4>
                            <Modal.Footer>
                                <Button variant="secondary" onClick={this.closeModal}>
                                    Close
                                </Button>
                            </Modal.Footer>
                        </React.Fragment>
                        }
                    </Modal.Body>
                </Modal>
            </React.Fragment>
        )
    }
}

function mapStateToProps(state) {
    const { teachers } = state.teachers;
    const { groups } = state.groups;
    return {
        teachers, groups
    };
}

const connectedTeachers = connect(mapStateToProps)(SubjectAdd);
export  { connectedTeachers as SubjectAdd }
