import { subjectsService } from "./Subjects.service"
import { subjectsConstants } from "./Subjects.constants";

export const subjectsActions = {
    getAll,
    save,
    edit,
    _delete
};

function getAll() {
    return dispatch => {
        dispatch(request({ loading: true }));
        subjectsService.getAll()
            .then(
                subjects =>{
                    dispatch(success(subjects));
                },
                error => {
                    if(error.message)
                        error = error.message;
                    dispatch(failure(error));

                }
            );
    };

    function request(loading) { return { type: subjectsConstants.SUBJECTS_LOADING, loading } }
    function success(subjects) { return { type: subjectsConstants.SUBJECTS_LOADING_SUCCESS, subjects } }
    function failure(error) { return { type: subjectsConstants.SUBJECTS_LOADING_FAILURE, error } }
}



function save(subject){
    return {type: subjectsConstants.SUBJECTS_SAVED_NEW, subject}
}

function edit(subject){
    return {type: subjectsConstants.SUBJECTS_SAVE_EDIT, subject}
}

function _delete(subjectId){
    return {type: subjectsConstants.SUBJECTS_DELETE, subjectId}
}