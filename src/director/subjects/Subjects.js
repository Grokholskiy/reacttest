import React from 'react';
import { subjectsActions } from './Subjects.action';
import { subjectsService } from './Subjects.service';
import {connect} from "react-redux";
import  Pagination from  '../../common/Pagination'
import { SubjectAdd } from './Subject.add';
import { Table } from "react-bootstrap";
import { DeleteConfirm } from '../../common/modal/DeleteConfirm';
import {Loading} from "../../common/loading/Loading";
import {Error} from "../../common/error/Error";
import {LangRoute} from "../../languages/LangRoute";


class Subjects extends React.Component {

    constructor(props){
        super(props);
        this.state = {
            offset:0,
            pageLimit:10,
            edit:false,
            add:false,
            editedSubject:{},
            deletedSubject:{}
        }

        this.closeModal = this.closeModal.bind(this);
        this.showEditModal = this.showEditModal.bind(this);
        this.deleteSubject = this.deleteSubject.bind(this);
    }

    showEditModal = (teacher) => {
        this.setState({editedSubject: teacher,add:false, edit:true});
    }

    showDeleteModal = (deletedSubject, show) => {
        this.setState({_delete:show, deletedSubject});
    }

    deleteSubject(subjectId){
        subjectsService._delete(subjectId)
        .then(() => {
            const { dispatch } = this.props;
            dispatch(subjectsActions._delete(subjectId));
            this.closeModal();
        }).catch(error => {
            console.log(error);
        });
    }

    closeModal = () => {
        this.setState({edit:false, add:false, _delete:false, editedSubject:{},deletedSubject:{}});
    }

    onPageChanged = data => {
        const { currentPage, totalPages, pageLimit } = data;
        const offset = (currentPage - 1) * pageLimit;
        this.setState({ currentPage, totalPages, offset });
    }

    componentDidMount(){
        // const { dispatch } = this.props;
        // dispatch(subjectsActions.getAll());
    }

    render() {
        const {loading, error, subjects} = this.props;
        const { pageLimit, offset,edit, editedSubject, _delete, deletedSubject} = this.state;

        if(loading){
            return  <Loading />
        }else if(error){
            return <Error error={error} />
        }

        return (
            <div className="d-flex flex-column align-items-center">
                <h1>Subjects Page</h1>
                    <Table striped bordered hover size="sm" className="col-sm-11 col-md-10 col-xl-8">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Name</th>
                                <th>Group</th>
                                <th>Teacher</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                        {subjects &&
                            subjects.slice(offset, offset + pageLimit).map((subject) => {
                                return <tr key={subject.id}>
                                    <td>{subject.id}</td>
                                    <td>{subject.name}</td>
                                    <td>{subject.group &&
                                            <>
                                                {subject.group.name}
                                            </>
                                        }
                                    </td>

                                    <td>{subject.teacher &&
                                            <>
                                                {subject.teacher.name} {subject.teacher.lastName}
                                            </>
                                        }
                                    </td>
                                    <td className="action">
                                        <div className="editBtn" onClick={() => {this.showEditModal(subject)}} title="Edit"></div>
                                        <div className="delBtn" onClick={() => {this.showDeleteModal(subject, true)}} title="Delete"></div>
                                    </td>
                                </tr>
                            })
                        }
                        </tbody>
                    </Table>
                    {subjects.length > 10 &&
                    <div className="d-flex flex-row py-4 justify-content-center">
                        <Pagination totalRecords={subjects.length} pageLimit={10} pageNeighbours={1} onPageChanged={this.onPageChanged} />
                    </div>
                    }

                    {edit &&
                        <SubjectAdd action="edit" data={editedSubject} onClose={this.closeModal}/>
                    }

                    {_delete &&
                    <DeleteConfirm title={`Confirm deleting ${deletedSubject.name}`}
                                   deleteObj={deletedSubject}
                                   confirmAction={this.deleteSubject}
                                   cancelAction={this.closeModal} />
                    }

                    <LangRoute path="/director/subject/add" render={(props) => <SubjectAdd {...props} action="add" data={{}} onClose={this.closeModal}/> } />
            </div>
        );
    }
}


function mapStateToProps(state) {
    const { subjects, loading , error} = state.subjects;
    return {
        subjects, loading, error
    };
}
const connectedTeachers = connect(mapStateToProps)(Subjects);
export  { connectedTeachers as Subjects }
