import  { subjectsConstants } from './Subjects.constants';

const initialState = {loading: false, subjects:[], error:'', edit:false, add:false}

export function SubjectsReducer(state = initialState, action) {
    switch (action.type) {
        case subjectsConstants.SUBJECTS_LOADING:
            return {
                error: '',
                loading: true,
                subjects:[]
            };
        case subjectsConstants.SUBJECTS_LOADING_SUCCESS:
            return {
                error: '',
                loading: false,
                subjects: action.subjects
            };
        case subjectsConstants.SUBJECTS_LOADING_FAILURE:
            return {
                error: action.error,
                loading: false,
                subjects: []
            }
        case subjectsConstants.SUBJECTS_SAVED_NEW:
            return {
                error: '',
                loading: false,
                subjects: [...state.subjects, action.subject]
            }
        case subjectsConstants.SUBJECTS_ADD_NEW:
            return {
                ...state,
                edit:false,
                add:true
            }
        case subjectsConstants.SUBJECTS_SAVE_EDIT:
            return {
                ...state,
                subjects: state.subjects.map((subject) => subject.id === action.subject.id ? action.subject : subject)
            }
        case subjectsConstants.SUBJECTS_DELETE:
            return {
                ...state,
                subjects: state.subjects.filter((subject) => subject.id !== action.subjectId)
            }

        default:
            return state
    }
}




