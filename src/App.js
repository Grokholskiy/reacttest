import React from 'react';
import './App.css';
import 'bootstrap/dist/css/bootstrap.css';
import { createStore, applyMiddleware } from 'redux';
import thunkMiddleware from 'redux-thunk';
import {connect, Provider} from 'react-redux';
import { combineReducers } from 'redux';
import { authentication } from './login/login.reducer';
import { StudentsReducer } from './director/students/Students.reducer';
import { TeachersReducer } from "./director/teachers/Teachers.reducer";
import { GroupsReducer } from './director/groups/Groups.reducer';
import  { SubjectsReducer  } from "./director/subjects/Subjects.reduser";
import { composeWithDevTools } from 'redux-devtools-extension';
import { LanguagesReducer } from "./languages/Languages.reducer";
import { AppConnected } from "./AppConnected";



export const rootReducer = combineReducers({
    authentication,
    students:StudentsReducer,
    teachers:TeachersReducer,
    groups:GroupsReducer,
    subjects:SubjectsReducer,
    language:LanguagesReducer
});

const composeEnhancers = composeWithDevTools({});
export const middlewares = [thunkMiddleware];

const store = createStore(
    rootReducer,
    composeEnhancers(
        applyMiddleware(
            ...middlewares
    ))

);



function App() {
  return (
      <Provider store={store}>
          <AppConnected />
      </Provider>
  );
}

export default App;