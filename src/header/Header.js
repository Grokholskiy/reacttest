import React  from 'react';
import './Header.css';
import { Link } from "react-router-dom";
import {Languages} from "../languages/Languages";
import FormattedMessage from "react-intl/lib/components/message";

function Header(props) {

    return (
        <nav>
            <ul>
                {props.subHeader &&
                    <props.subHeader />
                }
                <li>
                    <Link to="/logout">
                        <FormattedMessage id="logout"></FormattedMessage>
                    </Link>
                </li>
                <li>
                    <Link to="/test">Test</Link>
                </li>
                <li>
                    <Languages />
                </li>

            </ul>
        </nav>
    )
}

export { Header };


