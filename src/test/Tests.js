import "./Test.css"
import React from 'react';
import { Route, Link } from "react-router-dom";
import PureComponent from "./PureComponent";
import {Header} from "../header/Header";
import {DirectorHeader} from "../director/DirectorHeader";
import NotLazyComponent from "./NotLazyComponent";
import PortalComponent from "./PortalComponent";
import CookiePolicy from "./CookiePolicy";
import PropsTypeComponent from "./PropsTypeComponent";

export const Tests = () =>{

    return(
        <div>
            <Header subHeader={ DirectorHeader } />
            <ul>
                <li>
                    <Link to="/test/pure_component"> Pure component</Link>
                </li>
                <li>
                    <Link to="/test/lazy_component"> Lazy component</Link>
                </li>
                <li>
                    <Link to="/test/portal">Portal component</Link>
                </li>
                <li>
                    <Link to="/test/prop_types">Props Type component</Link>
                </li>

            </ul>
            <div className="d-flex flex-column align-items-center">
                <Route path="/test/pure_component" component={ PureComponent } />
                <Route path="/test/lazy_component" component={ NotLazyComponent } />
                <Route path="/test/portal" render={(props) => (
                    <PortalComponent>
                        <CookiePolicy />
                    </PortalComponent>
                )} />
                <Route path="/test/prop_types" component={ PropsTypeComponent } />
            </div>
        </div>
    )
}