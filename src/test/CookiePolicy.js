import React, { useState } from "react";

const CookiePolicy = (props)=>{
    const [accepted, setAccepted] = useState(getAcceptCookie());
    function acceptCookie(){
        setAccepted(true);
        localStorage.setItem("CookieAccepted", "true");
    }

    function getAcceptCookie(){
        let val = localStorage.getItem("CookieAccepted");
        if(val) return true;
                return false
    }

    if(accepted){
        return null;
    }else
        return(
            <div className="cookie-accept-policy show">
                <span className="close-btn" onClick={acceptCookie}>x</span>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris neque felis,
                    sollicitudin quis sodales id, pellentesque a neque. Proin elit ex, varius eu dapibus et,
                    efficitur dictum velit. In quis lorem cursus, scelerisque augue pellentesque, malesuada risus.
                    Nam sed justo ultricies, accumsan justo sed, dictum sapien. Nulla ullamcorper sagittis arcu,
                    cursus vestibulum quam vehicula non. Integer volutpat accumsan neque sit amet feugiat.
                    Quisque sed enim sed ex pellentesque sollicitudin in at enim. Quisque ornare odio
                    a ligula lobortis vulputate. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                </p>
            </div>)

}

export default CookiePolicy;