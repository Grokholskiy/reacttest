import React, { Suspense } from 'react';
import {Loading} from "../common/loading/Loading";


const LazyComponent = React.lazy(() => {
    return new Promise(resolve => setTimeout(resolve, 5000))
                 .then(()=>import("./LazyComponent"));
} );

class NotLazyComponent extends React.PureComponent{
    constructor(props){
        super(props);
        this.state = {
            initLazy:false
        }

        this.initLazy = this.initLazy.bind(this);
    }

    initLazy(){
        this.setState({initLazy:true});
    }

    render(){
        const { initLazy } = this.state;

        return(
            <div>
                <h3>Lazy Component Test</h3>
                {initLazy &&
                <Suspense fallback={<Loading><span></span></Loading>}>
                        <LazyComponent />
                    </Suspense>
                }
                {!initLazy &&
                    <button onClick={this.initLazy}>Init Lazy</button>
                }

            </div>
        )
    }

}

export default NotLazyComponent;
