import React from "react";
import PropTypes from 'prop-types';

class PropsTypeComponent extends React.PureComponent {

    render() {

        return(
            <div>
                <h1>Props Type Test</h1>

                <TestPtopsType />

            </div>
        )
    }

}

class TestPtopsType extends React.PureComponent{

    render(){
        const {name, age, isOnline} = this.props;

        return (
            <div>
                <h3>Name - {name}</h3>
                <h3>Age - {age}</h3>
                <h3>Is {isOnline ? "Online" : "OffLine"}</h3>
            </div>
        )}
    }

TestPtopsType.propTypes = {
    requireName: PropTypes.string,
    requireAge: PropTypes.number,
    requireIsOnline: PropTypes.bool
}


export default PropsTypeComponent;