import React from "react";
import ReactDOM from "react-dom";


class PortalComponent extends React.PureComponent {

    render (){
        return ReactDOM.createPortal(this.props.children, document.querySelector("body"));
    }
}

export  default PortalComponent;