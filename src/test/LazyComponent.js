import React from 'react';

class LazyComponent extends React.PureComponent{
    render(){
        return(
            <div>
                <h2>Lazy Component</h2>
            </div>
        )
    }

}
export default LazyComponent;