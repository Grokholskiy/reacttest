import React from 'react';


class PureComponent extends React.PureComponent{
    constructor(props) {
        super(props);
        this.state = {
            items: ["one"],
            title: "PureComponent does shallow comparison"
        }

        this.addItem = this.addItem.bind(this);
        this.newItemRef = React.createRef();
        this.newTitleRef = React.createRef();

        this.changeTitle = this.changeTitle.bind(this);
        this.renderCount = 0;
    }

    addItem(){
        const newItem = this.newItemRef.current.value;
        this.state.items.push(newItem);
        this.setState({items: this.state.items});
        this.newItemRef.current.value = "";
    }

    changeTitle(){
        const newTitle = this.newTitleRef.current.value;
        this.setState({title: newTitle});
    }

    render() {
        const { title, items } = this.state;
        this.renderCount = this.renderCount + 1;
        return(
            <div>
                <h3>Pure Component Test. Render Count #{this.renderCount}</h3>
                <div>
                    <input className="" defaultValue="" ref={this.newItemRef}/><button onClick={this.addItem}>Add Item</button>
                </div>
                <div>
                    <input className="" defaultValue="" ref={this.newTitleRef}/><button onClick={this.changeTitle}>Change Title</button>
                </div>

                <h4>{title}</h4>

                <ul>
                    {items.map((item, i) => {
                        return <li key={i}>{item}</li>
                    })}

                </ul>

            </div>
        )
    }

}


export default PureComponent;