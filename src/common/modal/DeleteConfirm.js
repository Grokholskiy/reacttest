import {Button, Modal} from "react-bootstrap";
import React from "react";

export const DeleteConfirm = ({title, deleteObj, confirmAction, cancelAction}) => {

    const onConfirm = ()=>{
        confirmAction(deleteObj.id);
    }

    const onCancel = () =>{
        cancelAction();
    }

    return (
        <Modal show={true} onHide={onCancel}>
            <Modal.Header>
                <Modal.Title>{title}</Modal.Title>
            </Modal.Header>

            <Modal.Footer>
                <Button variant="secondary" onClick={onCancel}>
                    Cancel
                </Button>
                <Button variant="danger" onClick={onConfirm}>
                    Confirm
                </Button>
            </Modal.Footer>
        </Modal>
    )
}