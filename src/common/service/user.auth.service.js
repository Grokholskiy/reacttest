

export const userAuthService = {
    getAuthUser,
    setAuthUser,
    removeAuthUser,
    getUserRoles
}

const authName = "user";

function getAuthUser(){
    const user = localStorage.getItem(authName);
    if(user)
        return JSON.parse(user);
    return null;
}

function setAuthUser(user){
    localStorage.setItem(authName, JSON.stringify(user));
}

function removeAuthUser(){
    localStorage.removeItem(authName);
}


function getUserRoles(){
    const user = getAuthUser();
    if(user)
        return user.roles;
    return null;
}