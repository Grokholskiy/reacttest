import  { authHeader } from '../../common/AuthHeader';

export default class RequestOpt{

    method = "";
    headers = {};

    GET = () =>{
        this.method = "GET";
        return this;
    }

    PUT = () =>{
        this.method = "PUT";
        return this;
    }

    POST = () =>{
        this.method = "POST";
        return this;
    }

    PATCH = () =>{
        this.method = "PATCH";
        return this;
    }

    DELETE = () =>{
        this.method = "DELETE";
        return this;
    }

    JSON = () => {
        this.headers["Content-Type"] = "application/json";
        return this;
    }

    AUTH = (token = '') => {
        this.headers = {...this.headers,...authHeader(token)}
        return this;
    }

    BODY = (body) => {
        if(this.headers["Content-Type"] != "application/json"){
            this.body = body;
        }else{
            this.body = JSON.stringify(body);
        }
        return this;
    }

}