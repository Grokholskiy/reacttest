import { extend } from "../utils"


export const ResponseType = Object.freeze({
    JSON:   Symbol("JSON"),
    BLOB:  Symbol("BLOB"),
    VOID: Symbol("VOID")
});

export const handle = (fetch, _option = {}) => {
    const  option = {401:true, 403:true, responseType: ResponseType.JSON};
    extend(option, _option);

    const promise = new Promise((resolve, reject) =>{
        fetch.then(response =>{
            if(option["401"])
                handle_401(response, reject);
            if(option["403"])
                handle_403(response, reject);

            if(!response.ok){
                handle_400(response, reject);
                handle_500(response, reject);
            }else {
                if(option.responseType === ResponseType.JSON ){
                    response.json().then(obj =>{
                        resolve(obj);
                    });
                }else if(option.responseType === ResponseType.BLOB ){
                    response.blob().then(obj =>{
                        resolve(obj);
                    });
                }else{
                    resolve();
                }
            }
        })
    });

    return promise;
}


function handle_401(response, reject) {
    if(response.status === 401) {
        reject("Unauthorized request");
    }
}

function handle_403(response, reject) {
    if(response.status === 403){
        reject("You have No permission");
    }
}


function handle_400(response, reject) {
    if(response.status >= 400 && response.status < 500){
        response.json().then((error) => {
            reject(error);
        })
    }
}


function handle_500(response, reject) {
    if(response.status >= 500){
        response.json().then((error) => {
            reject(error);
        })
    }
}