import React from "react";
import "./ValidationError.css";

export const ValidationError = ({state, field}) => {
    if(state[field + "Error"]){
        return (
            <div className="error-msg">{state[field+"ErrorMsg"]}</div>
        )
    }else{
        return null;
    }
}
