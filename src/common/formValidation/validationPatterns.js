export const validation = {
    required,min,max,email
};

function required(val){
    return !!val;
}

function min(min){
    return (val) => val.length >= min;
}

function max(max){
    return (val)=> val.length <= max;
}

function email(val){
    const regExp = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return regExp.test(String(val).toLowerCase());
}

