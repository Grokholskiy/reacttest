import React from "react";

class FormValidation extends React.Component {
    constructor(props){
        super(props);
        this.state= {
            wasSubmitted: false
        }

        Object.keys(props.validation).map(key => {
            this.state[key+"Error"] = false;
            this.state[key+"ErrorMsg"] = "";
            this.state[key] = ""
        });

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChange(e) {
        const { name, value } = e.target;
        this.setState({ [name]: value },()=> {
            if(this.state.wasSubmitted){
                this.validateField(name);
            }
        });
    }

    validateField(field){
        const patterns = this.props.validation[field].patterns;
        return patterns.some((patternObj,i) =>{
            const isValid = patternObj.pattern.call(this.state, this.state[field]);
            if(!isValid){
                this.setState({[field+"Error"]:true});
                this.setState({[field+"ErrorMsg"]:patternObj.errorMsg});
                return true;
            }else{
                if(this.state[field+"Error"]){
                    this.setState({[field+"Error"]:false});
                    this.setState({[field+"ErrorMsg"]:""});
                }
            }
        });
    }

    serverValidationField(field, msg){
        this.setState({[field+"Error"]:true}) ;
        this.setState({[field+"ErrorMsg"]:msg});
    }

    validateFields(){
        let isFormValid = true;
        Object.keys(this.props.validation).map(field=>{
           if(this.validateField(field)){
               isFormValid = false;
           }
        });
        return isFormValid;
    }

    buildObject(){
        const obj = {};
        Object.keys(this.props.validation).forEach(field => {
            obj[field] = this.state[field];
        });
        return obj;
    }

    handleSubmit(e){
        e.preventDefault();
        this.setState({wasSubmitted:true});

        if(this.validateFields()){
            const formObj = this.buildObject();
            this.props.onSubmitFn(formObj)
                .catch(error => {
                    if(error.error === "Validation Error"){
                        error.errors.forEach(e =>{
                            this.serverValidationField(e.details, e.error);
                        })
                    }else{
                        this.setState({ error: error.details });
                    }
                })
        }
    }

    render(){
        return this.props.render({
            handleChange: this.handleChange,
            handleSubmit: this.handleSubmit,
            state:        this.state
        });
    }
}

export { FormValidation }