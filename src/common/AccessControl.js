import  React from "react";
import { userAuthService } from "./service/user.auth.service"

const checkPermissions = ( allowedPermissions ) => {
    const userPermissions = userAuthService.getUserRoles();
    if (allowedPermissions.length === 0) {
        return true;
    }

    return userPermissions.some(permission =>
        allowedPermissions.includes(permission)
    );
};


const AccessControl = ({ allowedPermissions,
                         children,
                         renderNoAccess}) => {
    const permitted = checkPermissions(allowedPermissions);

    if (permitted) {
        return children;
    }
    return renderNoAccess();
}

AccessControl.defaultProps = {
    allowedPermissions: [],
    permissions: [],
    renderNoAccess: () => null
};

export default AccessControl;