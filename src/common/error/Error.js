import React from 'react';
import "./Error.css";

export const Error = ({error}) => {

    return(
        <div className="error-block">
            <h1>Error:</h1>
            {error.error &&
            <h3>{error.error}</h3>
            }

            {error.message &&
            <h3>{error.message}</h3>
            }

            {typeof error === 'string' &&
            <h3>{error}</h3>
            }
        </div>
    )
}