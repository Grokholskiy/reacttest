import React from "react";

const NoAccess = () => {

    return (
        <div className="d-flex flex-column align-items-center">
            <img src="/images/no_access.gif" alt="NoAccess" className="img-fluid"/>
        </div>
    )
}

export default  NoAccess;