export function authHeader(token = '') {
    let currentUser = JSON.parse(localStorage.getItem('user'));

    if(token){
        return { Authorization: `Bearer ${token}` };
    }

    if (currentUser && currentUser.token) {
        return { Authorization: `Bearer ${currentUser.token}` };
    } else {
        return {};
    }
}