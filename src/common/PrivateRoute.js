
import React from 'react'
import { Route, Redirect } from 'react-router-dom'
import {connect} from "react-redux";


function PrivateRoute({ component: Component, role, ...rest }){

    return (
<Route {...rest} render={(props) => (
            rest.logged
                ? <Component {...props}  />
                : <Redirect to={{pathname: "/login",
                                from: props.location.pathname
                }} />
        )} />
    )
}

function mapStateToProps(state) {
    const { logged } = state.authentication;
    return {
        logged
    };
}

const connectedApp = connect(mapStateToProps)(PrivateRoute);
export { connectedApp as PrivateRoute };