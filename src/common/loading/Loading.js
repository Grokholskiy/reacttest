import React from 'react';
import './Loading.css';

export const Loading = (props) =>{

    return (
            <div className="loading-block">
                {!props.children &&
                    <h1>Loading
                        <span className="loading-dots"></span>
                    </h1>
                }
                {props.children &&
                    React.Children.map(props.children,  () => {
                            return (
                                <>
                                    Loading
                                    <span className="loading-dots"></span>
                                </>
                            );
                        })
                }
            </div>
        )
}