import   React from 'react';
import { userService } from './User.service';
import { Row, Col} from "react-bootstrap";
import { Avatar} from "./Avatar"
import './User.css'
import {Loading} from "../loading/Loading";
import {Error} from "../error/Error";

class User extends  React.Component {
    constructor(props){
        super(props);
        this.state = {
            loading:false,
            error:'',
            user:{},
            newImage:'',
            userImage:''
        }
        this.changeImage = this.changeImage.bind(this);
        this.uploadImage = this.uploadImage.bind(this);
    }

    componentDidMount() {
        const {id, user } = this.props.match.params;
        const mypage = this.props.mypage;

        this.setState({loading:true});
        userService.getUserById(`${user}/${mypage ? "mypage" : id}`)
            .then(user=>{
                this.setState({user, loading:false});
                if(user.image && user.image.id){
                    userService.getImage(user.image.id)
                        .then(img=>{
                            let urlCreator = window.URL || window.webkitURL;
                            let imageUrl = urlCreator.createObjectURL(img);
                            this.setState({userImage:imageUrl});
                        }).catch(error=>{
                            console.log(error);
                        });
                }
            }).catch(error=>{
                this.setState({loading:false, error });
        });
    }

    changeImage(e){
        let image = e.target.files[0];
        let reader = new FileReader();
    
        reader.onload = (e) =>{
            this.setState({newImage:e.target.result});
        }
        reader.readAsDataURL(image);
        this.image = image;
    }

    uploadImage(){
        if(this.image){
            const { user } = this.props.match.params;

            userService.uploadImage(this.image, user)
                .then(success=>{
                    this.setState({ userImage:this.state.newImage, newImage:'',});
                }).catch(error=>{
                    console.log(error);
                });
        }
    }


    render(){
        const { name, lastName, role} = this.state.user;
        const { newImage, userImage, loading, error,  user } = this.state;
        const { mypage } = this.props;

        const getImage = () =>{
            if(newImage){
                return newImage;
            }else{
                if(user.image && user.image.id){
                    return userImage;
                }else{
                    return "/images/avatar.png";
                }
            }
        }

        if(loading){
            return  <Loading />
        }else if(error){
            return <Error error={error} />
        }

        return (
            <div className="container-fluid">
                <Row>
                    <Col className="d-flex justify-content-center">
                        <Avatar width="250px" height="250px" src={getImage()}>
                        </Avatar>
                            {mypage &&
                                <>
                                    <label>
                                        <div className="edit-photo-btn" title="Change Photo"></div>
                                        <input type="file" onChange={this.changeImage} accept="image/*" name="image" style={{display:'none'}}/>
                                    </label>
                                    {newImage &&
                                        <div className="save-photo-btn" title="Save" onClick={this.uploadImage}></div>
                                    }
                                </>
                            }
                    </Col>
                </Row>
                <Row className="d-flex justify-content-center">
                    <div className="justify-content-center">
                        <h3 style={{textAlign:'center'}}>{role}</h3>
                        <h4>{name} {lastName}</h4>


                        { (role === "STUDENT" || role === "TEACHER") &&
                            <h4>Group: {user.group &&
                                user.group.name
                            }
                            </h4>

                        }
                        {role === "TEACHER" &&
                            <>
                                <h4 className="user-subjects">Subjects: {user.subjects &&
                                                    <ul>
                                                        {user.subjects.map(subject =>{
                                                            return <li key={subject.id}>{subject.name}</li>
                                                        })}
                                                    </ul>
                                                }
                                </h4>

                            </>
                        }

                    </div>
                </Row>
            </div>
        )
    }

}

export { User };