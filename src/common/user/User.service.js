import { authHeader } from "../AuthHeader";
import { apiUrl } from "../constants";
import {handle} from "../service/responceHendler";
import RequestOpt from "../service/requestOptionBuilder";
import { ResponseType } from "../service/responceHendler"

export const userService = {
    getUserById,
    uploadImage,
    getImage
}

function getUserById(id) {
    return handle(
        fetch(`${apiUrl}${id}`,new RequestOpt().GET().AUTH())
    );
}

function getImage(id){
    return handle(
        fetch(`${apiUrl}image/${id}`,new RequestOpt().GET().AUTH()),
        {responseType:ResponseType.BLOB}
    );
}

function uploadImage(image,user){
    let fd = new FormData();
    fd.append("image", image);
    return handle(
        fetch(`${apiUrl}${user}/image`,new RequestOpt().POST().AUTH().BODY(fd)),
        {responseType:ResponseType.VOID}
    );
}