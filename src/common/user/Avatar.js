import React from 'react';
import {Image} from "react-bootstrap";;

export const Avatar = (props)=>{

    const style = {
        width: props.width,
        height:props.height,
        overflow:'hidden'
    }
    const imageSrc = props.src;
    return (
        <div style={style}>
            <Image width={props.width} height={props.height} src={imageSrc} roundedCircle />
            {props.children}
        </div>
    )

}

