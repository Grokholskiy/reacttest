const extend = (source, obj) => {
    for (let i in obj) {
        if (source.hasOwnProperty(i)) {
            source[i] = obj[i];
        }
    }
};

export { extend };


const queryParam = (url, params = []) =>{
    let response = {};
    const urlParams = new URLSearchParams(url.search);

    params.map((param)=>{
        response[param] = urlParams.get(param);
    });

    return response;
}

export { queryParam };