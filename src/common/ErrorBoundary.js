import React, { Component } from 'react';

const style = {
    display: 'flex',
    justifyContent: 'center'
};

class ErrorBoundary extends Component {
    constructor(props) {
        super(props);
        this.state = {
            hasError: false
        };
    }
    componentDidCatch(error, info) {
        this.setState({
            hasError: true
        });
    }
    render() {
        if(this.state.hasError) {
            return (
                <div style={style}>
                    <img src="/images/opps.png" />
                </div>
            );
        }

        return this.props.children;
    }
}

export default ErrorBoundary;