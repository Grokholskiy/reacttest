export const apiUrl = "https://localhost:8443/";
//export const apiUrl = "http://192.168.157.209:8080/";

export const ACCESS_TOKEN = 'accessToken';
export const OAUTH2_REDIRECT_URI = 'http://localhost:3000/login/'
export const FACEBOOK_AUTH_URL = apiUrl + 'oauth2/authorize/facebook?redirect_uri=' + OAUTH2_REDIRECT_URI;
export const week = ["Monday", "Tuesday","Wednesday", "Thursday", "Friday", "Saturday"];
export const maxLesson = new Array(8).fill(0);
export const marks = [{key:"TWO",value:2},{key:"THREE",value:3}, {key:"FOUR",value:4},{key:"FIVE",value:5},{key:"WS",value:"ws"}]