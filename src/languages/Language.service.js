
export const langService = {
    get:currentLanguage,
 change:changeLanguage
}

const langKey = "lang"

function currentLanguage(){
    const currentLang = localStorage.getItem(langKey);
    if(currentLang){
        return currentLang;
    }
    return null;
}

function changeLanguage(lang){
    localStorage.setItem(langKey, lang);
}