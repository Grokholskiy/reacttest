import { languagesActionConst } from "./Languages.constants";
import {langService} from "./Language.service";

export const languageAction = {
    change
}

function change(language) {
    langService.change(language.lang);
    return {
        type: languagesActionConst.LANGUAGE_CHANGING, language
    }
}