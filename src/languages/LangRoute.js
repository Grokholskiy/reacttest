import React from "react";
import { langService } from "./Language.service";
import { PrivateRoute } from "../common/PrivateRoute";
import { Route } from "react-router-dom";


class LangRoute extends React.Component{
    constructor(props){
        super(props);
    }

    render(){
        const {path, privateRoute, component:Component} = this.props;
        const currentLang = langService.get();
        let newPath = path;

        if(currentLang){
            newPath = `/:locale${path}`;
        }


        if(privateRoute){
            return (<PrivateRoute {...this.props} path={newPath} component={Component}  />)
        }else{
            return (<Route {...this.props} path={newPath} component={Component} />)
        }
    }
}

export { LangRoute }