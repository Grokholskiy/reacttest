import React from "react";
import {lang_list} from "./Languages.constants";
import {connect} from "react-redux";
import {languageAction} from "./Languages.action";
import './Language.css';
import { createBrowserHistory } from 'history';

class Languages extends React.Component {
    constructor(props){
        super(props);
        this.changeLang = this.changeLang.bind(this);

    }

    changeLang(lang){
        const { dispatch }= this.props;
        const history = createBrowserHistory();
        let location = history.location;
        dispatch(languageAction.change({lang,url:location.pathname}));
    }

    render(){
        const { language } = this.props;
        return (
            <ul className="lang-menu">
                {lang_list.map(lang => {
                      return <li key={lang} className={language === lang ? "current":""}>
                            <span onClick={()=>{this.changeLang(lang)}}>{lang}</span>
                        </li>
                    })
                }
            </ul>
        );
    }
}


function mapStateToProps(state) {
    const { language } = state.language;
    return {
        language
    };
}

const connectedLanguages = connect(mapStateToProps)(Languages);
export { connectedLanguages  as Languages }