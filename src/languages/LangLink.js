import React from "react";
import { langService } from "./Language.service";
import  { NavLink, Link  } from "react-router-dom";

class LangLink extends React.Component{
    constructor(props){
        super(props);
    }

    render() {
        const currentLang = langService.get();
        const {to, activeClassName} = this.props;

        let langTo = to;
        if(currentLang){
            langTo = `/${currentLang}${to}`;
        }

        if (activeClassName) {
            return (<NavLink to={langTo} activeClassName={activeClassName}>{this.props.children}</NavLink>)
        } else {
            return (<Link to={langTo}>{this.props.children}</Link>)
        }
    }
}

export  { LangLink }