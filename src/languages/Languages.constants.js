// import React from "react";

// const messages_uk = React.lazy( ()=>{ import("../translations/uk")});
// const messages_en = React.lazy( ()=>{ import("../translations/en")});

import messages_uk from "../translations/uk";
import messages_en from "../translations/en";

export function getMessages(lang){
    switch (lang) {
        case "uk":{
            return  messages_uk;
        }
        case "en":{
            return messages_en;
        }
    }
};

export const default_lang = "en";
export const lang_list = ["uk", "en"];

export const languagesActionConst = {
    LANGUAGE_CHANGING:"LANGUAGE_CHANGING",
    LANGUAGE_INIT:"LANGUAGE_INIT"
}