import { languagesActionConst as type} from "./Languages.constants";
import { langService } from "./Language.service";
import { default_lang } from "./Languages.constants";

let currentLang = langService.get();
if(!currentLang){
    currentLang = navigator.language.split(/[-_]/)[0];
}

if(!currentLang){
    currentLang = default_lang;
}

const initialState = {language: currentLang, url:''};

export function LanguagesReducer(state = initialState, action) {
    switch (action.type){
        case type.LANGUAGE_CHANGING:
            return {
                language: action.language.lang,
                url: action.language.url
            }

            default:
                return state
    }
}