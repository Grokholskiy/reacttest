import {Registration} from "./registration/Registration";
import {PrivateRoute} from "./common/PrivateRoute";
import {Director} from "./director/Director";
import {Teacher} from "./teacher/Teacher";
import {Student} from "./student/Student";
import {User} from "./user/User";
import {Login, Logout} from "./login/Login";
import {LoginSocila} from "./login/LoginSocial";
import {Tests} from "./test/Tests";
import {Home} from "./home/Home";
import {BrowserRouter as Router, Route, Switch} from "react-router-dom";
import {IntlProvider} from "react-intl";
import {getMessages} from "./languages/Languages.constants";
import React from "react";
import { Redirect } from "react-router-dom";
import {connect} from "react-redux";


function AppConnected(props) {
    const mainRoutes = (locale) =>(
        <>
            <Route        path={`${locale}/registration`}  component={ Registration }/>
            <PrivateRoute path={`${locale}/director`}      component={ Director }  />
            <PrivateRoute path={`${locale}/teacher`}       component={ Teacher }  />
            <PrivateRoute path={`${locale}/student`}       component={ Student } />
            <PrivateRoute path={`${locale}/user`}          component={ User } />
            <Route        path={`${locale}/logout`}        component={ Logout } />
            <Route        path={`${locale}/login`} exact   component={ Login } />
            <Route        path={`${locale}/login/:social`} component={ LoginSocila } />
            <Route        path={`${locale}/test`}          component={ Tests } />
            <Route        path={`${locale}/`} exact              component={ Home } />
        </>
    )

    let redirectUrl = "";

    if(!props.url.startsWith(props.language) && props.url !== ""){
        const pathname = props.url.replace(/\/[a-zA-Z]{2}\//, "");
        if(pathname.startsWith("/")){
            redirectUrl = `/${props.language}${pathname}`;
        }else{
            redirectUrl = `/${props.language}/${pathname}`;
        }
    }

    return (
        <Router>
            {redirectUrl &&
              <Redirect to={redirectUrl} />
            }
            {console.log(getMessages(props.language))}
            {console.log(props.language)}
            <IntlProvider locale={props.language} messages={getMessages(props.language)} >
                <Switch>
                    <Route path="/:locale(en|uk)" render={ ({ match: { path }}) =>(
                        mainRoutes("/:locale")
                    )}/>

                    <Route path="/" render={() => (
                        mainRoutes("")
                    )}/>
                </Switch>
            </IntlProvider>
        </Router>
    );
}

function mapStateToProps(state) {
    const { language, url } = state.language;
    return {
        language, url
    };
}

const connectedApp = connect(mapStateToProps)(AppConnected);
export {connectedApp as AppConnected};