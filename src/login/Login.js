import React, { useEffect } from 'react';
import './Login.css';
import  { loginActions } from './login.action';
import { connect } from 'react-redux';
import  { Redirect } from  'react-router-dom';
import  { Link } from "react-router-dom";
import { FACEBOOK_AUTH_URL } from "../common/constants";
import fbLogo from "../image/login/fb-logo.png";
import {FormValidation} from "../common/formValidation/FormValidation";
import {ValidationError} from "../common/formValidation/ValidationError";
import {validation} from "../common/formValidation/validationPatterns";

class Login extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            username: '',
            password: '',
            changed:false
        };

        this.redirectTo = props.location.from || "/"

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChange(e) {
        const { name, value } = e.target;
        this.setState({ [name]: value, changed: true });
    }


    handleSubmit(object) {
        const { dispatch } = this.props;
        dispatch(loginActions.login(object));
    }

    render (){
        const { username, password, submitted, changed } = this.state;
        const { logged, loading, error }  = this.props;
        if(logged){
            return (
                <Redirect to={this.redirectTo} />
            )
        }

        return (
            <div className="login-page">
                <div className="wrapper fadeInDown">
                    <div id="formContent">

                        <div className="fadeIn first">
                            <h3>Login page</h3>
                        </div>

                        <FormValidation onSubmitFn={this.handleSubmit}
                                        validation={
                                            {
                                                username: {
                                                    patterns: [
                                                        {
                                                            pattern: validation.required,
                                                            errorMsg: "Enter login!"
                                                        }
                                                    ]
                                                },
                                                password: {
                                                    patterns: [
                                                        {
                                                            pattern: validation.required,
                                                            errorMsg: "Enter password"
                                                        }
                                                    ]
                                                }
                                            }
                                        }
                                render={(props) =>(
                                    <form name="form" onSubmit={props.handleSubmit}>
                                        <input type="text"
                                               id="login"
                                               className="fadeIn second"
                                               name="username"
                                               value={props.state.username}
                                               onChange={props.handleChange}
                                               autocomplete="off"
                                               placeholder="login" />
                                        <ValidationError state = {props.state} field={"username"}/>
                                        <input type="text"
                                               id="password"
                                               value={props.state.password}
                                               className="fadeIn third"
                                               onChange={props.handleChange}
                                               name="password"
                                               autocomplete="off"
                                               placeholder="password" />
                                        <ValidationError state = {props.state} field={"password"}/>
                                        <input type="submit" disabled={loading} className="fadeIn fourth" value="Log In" />
                                        <SocialLoginBtn />
                                    </form>
                                )}>


                        </FormValidation>

                        {error && !changed &&
                            <div className="error-block">
                                {error}
                            </div>
                        }
                        <div id="formFooter">
                            <a className="underlineHover" href="#">Forgot Password?</a>
                            <Link to="/registration" className="underlineHover"> Sing Up?</Link>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

class SocialLoginBtn extends React.Component {
    render() {
        return (
            <div className="social-login">
                <a className="btn btn-block social-btn facebook" href={FACEBOOK_AUTH_URL+"facebook"}>
                    <img src={fbLogo} alt="Facebook" /> Log in with Facebook</a>
            </div>
        );
    }
}

function Logout(props){

    useEffect(() => {
        props.dispatch(loginActions.logout());
    });
    const { logged } = props;

    return (
        <div>
            {!logged &&
                <Redirect to="/login" />
            }
        </div>
    );
}

function mapStateToProps(state) {
    const { user, loading , logged, error} = state.authentication;
    return {
        user, loading, logged, error
    };
}
const connectedLogin = connect(mapStateToProps)(Login);
const connectedLogout = connect(mapStateToProps)(Logout);

export { connectedLogout as Logout };
export { connectedLogin as Login };
