import React from 'react';
import Adapter from 'enzyme-adapter-react-16';
import { shallow, configure } from 'enzyme';
import { Login } from "./Login";
import { testStore } from "../../utils/index"


configure({adapter: new Adapter()});


const setUp = (initialState={}) => {
    const store = testStore(initialState);
    const wrapper = shallow(<Login store={store} />).childAt(0).dive();
    return wrapper;
};

describe("Login Component", ()=>{

    it("It should render without errors", ()=>{
        const component = shallow(<Login />);
        console.log(component.debug());
        const wraper = component.find(".login-page");
        expect(wraper.length).toBy(1);
    })

});