import  { loginConstants } from './login.constants';

let user = JSON.parse(localStorage.getItem('user'));
const initialState = user ? { logged: true, user:user.username, loading:false, error:"" } : {logged:false, user:"",loading:false, error:""};

export function authentication(state = initialState, action) {
    switch (action.type) {
        case loginConstants.LOGIN_REQUEST:
            return {
                error: '',
                logged:false,
                user: '',
                loading: true
            };
        case loginConstants.LOGIN_SUCCESS:
            return {
                logged: true,
                user: action.username
            };
        case loginConstants.LOGIN_FAILURE:
            return {
                error: action.error,
                logged:false,
                user: '',
                loading: false
            };
        case loginConstants.LOGOUT_REQUEST:
            return {
                logged:false,
                user: '',
                error:''
            };
        default:
            return state
    }
}