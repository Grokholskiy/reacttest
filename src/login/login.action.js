import { loginConstants } from './login.constants';
import  { loginService } from './login.service';

export const loginActions = {
    login,
    logout,
    socialLogin
};

function login(user) {
    return dispatch => {
        dispatch(requestLogin({ loading: true }));

        loginService.login(user)
            .then(
                user => {
                    dispatch(successLogin(user.username));
                },
                error => {
                    if(error.message)
                        error = error.message;
                    dispatch(failureLogin(error));
                }
            );
    };
}

function socialLogin(token) {
    return dispatch => {
        dispatch(requestLogin({ loading: true }));

        loginService.socialLogin(token)
            .then(
                user => {
                    dispatch(successLogin(user.username));
                },
                error => {
                    if(error.message)
                        error = error.message;
                    dispatch(failureLogin(error));
                }
            );
    };
}

function requestLogin(loading) { return { type: loginConstants.LOGIN_REQUEST, loading } }
function successLogin(username) { return { type: loginConstants.LOGIN_SUCCESS, username } }
function failureLogin(error) { return { type: loginConstants.LOGIN_FAILURE, error } }

function logout() {
    loginService.logout();
    return { type: loginConstants.LOGOUT_REQUEST };
}