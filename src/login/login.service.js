import  { apiUrl } from "../common/constants";
import  { userAuthService } from '../common/service/user.auth.service'
import {handle} from "../common/service/responceHendler";
import RequestOpt from "../common/service/requestOptionBuilder";

export const loginService = {login, logout, socialLogin};

function login(user) {
    return handle(
        fetch(`${apiUrl}auth/singin`,new RequestOpt().POST().JSON().BODY(user))
    ).then(user => {
        if (user && user.token) {
            userAuthService.setAuthUser(user);
        }
        return user;
    })
}


function socialLogin(token) {
    return handle(
        fetch(`${apiUrl}user_info`,new RequestOpt().POST().AUTH(token))
    ).then(user => {
        if (user) {
            user.token = token;
            userAuthService.setAuthUser(user);
        }
        return user;
    })
}


function logout() {
    userAuthService.removeAuthUser();
}
