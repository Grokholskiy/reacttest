import React from "react";
import { connect } from "react-redux";
import { Redirect } from  'react-router-dom';
import { queryParam } from "../common/utils";
import { loginActions } from "./login.action";
import {Loading} from "../common/loading/Loading";
import {Error} from "../common/error/Error";


class LoginSocila extends React.Component {
    constructor(props){
        super(props);
    }

    componentWillMount(){
        const { dispatch, location } = this.props;
        const paramsObject= queryParam(location, ["token"]);

        if(paramsObject.token){
            dispatch(loginActions.socialLogin(paramsObject.token));
        }
    }

    render(){
        const {loading, logged, error, history} = this.props;

        if(logged){
            history.replace(history.location.pathname, '/' );
            return (
                <Redirect to={"/"} />
            )
        }

        if(loading){
            return  <Loading />
        }else if(error){
            return <Error error={error} />
        }

        return null;
    }
}


function mapStateToProps(state) {
    const { user, loading , logged, error} = state.authentication;
    return {
        user, loading, logged, error
    };
}
const connectedLoginSocila = connect(mapStateToProps)(LoginSocila);
export { connectedLoginSocila as LoginSocila };